# <a href="https://tripetto.com/"><img src="https://unpkg.com/tripetto/assets/banner.svg" alt="Tripetto"></a>

Tripetto is a full-fledged form kit. Rapidly build and run smart flowing forms and surveys. Drop the kit in your codebase and use all of it or just the parts you need. The visual [**builder**](https://www.npmjs.com/package/tripetto) is for form building, and the [**runners**](https://www.npmjs.com/package/tripetto-runner-foundation) are for running those forms in different UI variants. It is entirely extendible and customizable. Anyone can build their own building [**blocks**](https://docs.tripetto.com/guide/blocks) (e.g., question types) or runner UI's.

# Autoscroll runner
[![Version](https://img.shields.io/npm/v/tripetto-runner-autoscroll.svg)](https://www.npmjs.com/package/tripetto-runner-autoscroll)
[![License](https://img.shields.io/npm/l/tripetto-runner-autoscroll.svg)](https://opensource.org/licenses/MIT)
[![Downloads](https://img.shields.io/npm/dt/tripetto-runner-autoscroll.svg)](https://www.npmjs.com/package/tripetto-runner-autoscroll)
[![Pipeline status](https://gitlab.com/tripetto/runners/autoscroll/badges/master/pipeline.svg)](https://gitlab.com/tripetto/runners/autoscroll/commits/master)
[![Follow us on Twitter](https://img.shields.io/twitter/follow/tripetto.svg?style=social&label=Follow)](https://twitter.com/tripetto)

# Purpose
This package is a UI for running Tripetto forms and surveys that uses a scroll effect (either vertical or horizontal) to display the questions and elements (so called blocks).

[![Try the demo](https://unpkg.com/tripetto/assets/button-demo.svg)](https://tripetto.gitlab.io/runners/autoscroll/)

# Get started
There a multiple options how you can use this runner. From plain old HTML to [React](https://reactjs.org/) or using imports.

## Option A: Embed in HTML using CDN
You need to import the [runner foundation](https://www.npmjs.com/package/tripetto-runner-foundation) script and the autoscroll runner script:
```html
<script src="https://unpkg.com/tripetto-runner-foundation"></script>
<script src="https://unpkg.com/tripetto-runner-autoscroll"></script>
<script>
TripettoAutoscroll.run({
    definition: /** Supply a form definition here. */,
    onSubmit: function(instance) {
      // Implement your response handler here.

      // For this example we output all exportable fields to the browser console
      console.dir(TripettoRunner.Export.exportables(instance));

      // Or output the data in CSV-format
      console.dir(TripettoRunner.Export.CSV(instance));
    }
});
</script>
```

[![Try demo on CodePen](https://unpkg.com/tripetto/assets/button-codepen.svg)](https://codepen.io/tripetto/pen/KEYrVm)

## Option B: Using React
1. Install the required packages from npm:
```bash
$ npm install tripetto-runner-foundation tripetto-runner-autoscroll react react-dom
```
2. Use the React component:
```typescript
import ReactDOM from "react-dom";
import { AutoscrollRunner } from "tripetto-runner-autoscroll";
import { Export } from "tripetto-runner-foundation";

ReactDOM.render(
  <AutoscrollRunner
    definition={/** Supply a form definition here. */}
    onSubmit={instance => {
      // Implement your response handler here.

      // For this example we output all exportable fields to the browser console
      console.dir(Export.exportables(instance));

      // Or output the data in CSV-format
      console.dir(Export.CSV(instance));
    }}
    />,
  document.getElementById("your-element")
);
```

## Option C: Import from npm
1. Install the required packages from npm:
```bash
$ npm install tripetto-runner-foundation tripetto-runner-autoscroll react react-dom
```
2. Import the runner:
```typescript
import { run } from "tripetto-runner-autoscroll";
import { Export } from "tripetto-runner-foundation";

run({
    definition: /** Supply a form definition here. */,
    onSubmit: instance => {
      // Implement your response handler here.

      // For this example we output all exportable fields to the browser console
      console.dir(Export.exportables(instance));

      // Or output the data in CSV-format
      console.dir(Export.CSV(instance));
    }
});
```

# Documentation
The complete Tripetto documentation can be found at [docs.tripetto.com](https://docs.tripetto.com).

# Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/runners/autoscroll/issues) and we'll look into them.

For general support contact us at [support@tripetto.com](mailto:support@tripetto.com). We're more than happy to assist you.

# License
[![License](https://img.shields.io/npm/l/tripetto.svg)](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)

You are allowed to use this package without a license (and free of charge) as long as you keep the Tripetto branding enabled (which is the default mode). When you want to remove the branding, you need a (paid) license to do so.

# Contributors
- [Hisam Fahri](https://gitlab.com/hisamafahri) (Indonesian translation)
- [Krzysztof Kamiński](https://gitlab.com/kriskaminski) (Polish translation)

# About us
If you want to learn more about Tripetto or contribute in any way, visit us at [Tripetto.com](https://tripetto.com/).
