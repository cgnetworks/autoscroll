import typescript from "@rollup/plugin-typescript";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import { terser } from "rollup-plugin-terser";
import { visualizer } from "rollup-plugin-visualizer";
import commonjs from "@rollup/plugin-commonjs";
import replace from "@rollup/plugin-replace";
import alias from "@rollup/plugin-alias";
import pkg from "./package.json";

const replacePlugin = replace({
    preventAssignment: true,
    values: {
        PACKAGE_NAME: JSON.stringify(pkg.name),
        PACKAGE_TITLE: JSON.stringify(pkg.title),
        PACKAGE_VERSION: JSON.stringify(pkg.version),
    },
});

const typescriptPlugin = typescript({
    target: "ES6",
    module: "ES6",
    removeComments: true,
});

const terserPlugin = terser({
    format: {
        preamble: `/*! ${require("./tasks/banner/banner.js")} */`,
        comments: false,
    },
});

const onwarn = (warning, rollupWarn) => {
    if (warning.code !== "CIRCULAR_DEPENDENCY") {
        rollupWarn(warning);
    }
};

export default [
    {
        input: ["./src/index.ts"],
        output: [
            {
                file: "./runner/esm/index.mjs",
                format: "esm",
                exports: "named",
            },
        ],
        external: ["tripetto-runner-foundation", "react", "react-dom", "react/jsx-runtime.js"],
        plugins: [
            alias({
                entries: {
                    "react/jsx-runtime": "react/jsx-runtime.js"
                }
            }),
            replacePlugin,
            typescriptPlugin,
            nodeResolve(),
            commonjs(),
            terserPlugin,
            visualizer({
                filename: "./reports/bundle-runner-esm.html",
            }),
        ],
        onwarn,
    },
    {
        input: ["./src/builder/index.ts"],
        output: [
            {
                file: "./builder/esm/index.mjs",
                format: "esm",
                exports: "named",
            },
        ],
        external: ["tripetto"],
        plugins: [
            alias({
                entries: {
                    "@l10n":  "builder/l10n/index.mjs"
                },
            }),
            typescriptPlugin,
            nodeResolve(),
            terserPlugin,
            visualizer({
                filename: "./reports/bundle-builder-esm.html",
            }),
        ],
        onwarn,
    },
];
