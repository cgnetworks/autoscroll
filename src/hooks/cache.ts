import { ReactNode, useRef } from "react";
import { IObservableNode, TStatus, each } from "tripetto-runner-foundation";
import { TRunnerStatus } from "tripetto-runner-react-hook";
import { IAutoscrollRendering } from "@interfaces/block";

export interface ICache {
    readonly fetch: (
        create: () => ReactNode,
        node: IObservableNode<IAutoscrollRendering>,
        status: TRunnerStatus | TStatus,
        isActivated: boolean,
        tabIndex: number
    ) => ReactNode;

    readonly purge: () => void;
}

export function useCache(): ICache {
    const cacheRef = useRef<ICache>();

    if (!cacheRef.current) {
        const cache: {
            [node: string]:
                | {
                      readonly buffer: ReactNode;
                      readonly status: TRunnerStatus | TStatus;
                      readonly isActivated: boolean;
                      readonly isPassed: boolean;
                      readonly tabIndex: number;
                  }
                | undefined;
        } = {};

        cacheRef.current = {
            fetch: (
                create: () => ReactNode,
                node: IObservableNode<IAutoscrollRendering>,
                status: TRunnerStatus | TStatus,
                isActivated: boolean,
                tabIndex: number
            ) => {
                const key = node.key;
                const current = !node.hasChanged() && cache[key];

                if (
                    current &&
                    current.isActivated === isActivated &&
                    current.status === status &&
                    current.isPassed === node.isPassed &&
                    current.tabIndex === tabIndex
                ) {
                    return current.buffer;
                }

                return (cache[key] = {
                    buffer: create(),
                    status,
                    isActivated,
                    isPassed: node.isPassed,
                    tabIndex,
                }).buffer;
            },
            purge: () => {
                each(
                    cache,
                    (node, key: string) => {
                        delete cache[key];
                    },
                    {
                        keys: true,
                    }
                );
            },
        };
    }

    return cacheRef.current;
}
