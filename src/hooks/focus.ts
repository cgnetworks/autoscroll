import { MutableRefObject, useEffect, useRef, useState } from "react";
import { DateTime, TAny, cancelUIInterval, extendImmutable, scheduleUIInterval, scheduleUITimeout } from "tripetto-runner-foundation";
import { IViewport } from "@interfaces/viewport";
import { isIn } from "@helpers/isin";

export interface IFocusRef {
    readonly key: string;
}

export interface IFocus {
    [key: string]: boolean | undefined;
}

export type TAutoFocus = (ref: IFocusRef, isActivated: boolean, on?: (el: HTMLElement) => void) => (el: HTMLElement | null) => void;

export const useFocus = (props: {
    readonly viewportRef: React.MutableRefObject<IViewport>;
    readonly gainFocus?: boolean;
    readonly initialFocus?: IFocus;
    readonly onFocus?: (key: string) => void;
    readonly hooks?: {
        hook: (type: "restart", on: () => void) => void;
        onInteraction: () => void;
    };
}) => {
    const [focus, setFocus] = useState<IFocus>(props.initialFocus || {});
    const frameRef = useRef<HTMLIFrameElement>();
    const gainRef = useRef(true);
    const autoFocusRef = useRef<string>();
    const activeRef = useRef<string>();
    const elementRef = useRef<HTMLElement>();
    const elementsRef = useRef<{
        [key: string]: HTMLElement | undefined;
    }>({});

    useEffect(() => {
        const handle = scheduleUIInterval(() => {
            const focusElement = (autoFocusRef.current && elementsRef.current[autoFocusRef.current]) || undefined;

            if (focusElement && (gainRef.current || autoFocusRef.current !== activeRef.current) && frameRef.current) {
                if (DateTime.elapsed(props.viewportRef.current.timestamp, true) < 100 || !isIn(focusElement, props.viewportRef)) {
                    return;
                }

                cancelUIInterval(handle);

                if (document.hasFocus()) {
                    const activeElement = document.activeElement;
                    let allowFocus = activeElement && activeElement.isEqualNode(frameRef.current);

                    if (props.gainFocus && gainRef.current) {
                        allowFocus = allowFocus || !activeElement || activeElement.tagName === "BODY";
                    }

                    gainRef.current = false;
                    activeRef.current = autoFocusRef.current;

                    if (allowFocus) {
                        focusElement.focus({
                            preventScroll: true,
                        });
                    }
                }

                delete elementsRef.current[autoFocusRef.current!];
            } else {
                cancelUIInterval(handle);
            }
        }, 100);

        return () => {
            cancelUIInterval(handle);
        };
    });

    return [
        frameRef as MutableRefObject<HTMLIFrameElement>,
        (ref: IFocusRef, hasFocus: boolean, on?: () => void) => (e: React.FocusEvent) => {
            if (hasFocus && gainRef.current) {
                gainRef.current = false;
            }

            setFocus(
                extendImmutable(focus, {
                    [ref.key]: hasFocus,
                })
            );

            if (hasFocus) {
                elementRef.current = e.target as HTMLElement;
                autoFocusRef.current = activeRef.current = ref.key;

                if (props.onFocus) {
                    props.onFocus(ref.key);
                }
            }

            if (on && focus[ref.key] !== hasFocus) {
                on();
            }
        },
        (ref: IFocusRef, isActivated: boolean, forwardRef?: (el: HTMLElement) => void) => (element: HTMLElement | null) => {
            if (isActivated && element) {
                elementsRef.current[ref.key] = element;
                autoFocusRef.current = ref.key;

                if (forwardRef) {
                    forwardRef(element);
                }
            } else {
                delete elementsRef.current[ref.key];
            }
        },
        (
            ref: IFocusRef & {
                readonly filterInputs?: boolean;
            }
        ) => {
            if (
                ref.filterInputs &&
                (!elementRef.current || (elementRef.current.tagName !== "INPUT" && elementRef.current.tagName !== "TEXTAREA"))
            ) {
                return false;
            }

            return focus[ref.key];
        },
        () =>
            new Promise((fnResolve: () => void) => {
                if (activeRef.current) {
                    const element = elementsRef.current[activeRef.current];

                    if (element) {
                        element.blur();

                        scheduleUITimeout(() => fnResolve(), 100);

                        return;
                    }
                }

                fnResolve();
            }),
        () => setFocus({}),
        focus,
    ] as [
        MutableRefObject<HTMLIFrameElement>,
        (ref: IFocusRef, hasFocus: boolean, on?: () => void) => (e: React.FocusEvent) => TAny,
        TAutoFocus,
        (
            ref: IFocusRef & {
                readonly filterInputs?: boolean;
            }
        ) => boolean | undefined,
        () => Promise<void>,
        () => void,
        IFocus
    ];
};
