import styled from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { TRunnerViews } from "tripetto-runner-react-hook";
import { L10n, isBoolean } from "tripetto-runner-foundation";
import { color } from "tripetto-runner-fabric/color";
import { SIZE } from "@ui/const";

export const BannerElement = styled.div<{
    props: {
        styles: IRuntimeStyles;
        alignment: "left" | "center" | "right";
        visible?: boolean;
    };
}>`
    display: block;
    color: ${(ref) => color(ref.props.styles.font.color, (o) => o.manipulate((m) => m.alpha(0.4)))};
    margin-top: ${16 / SIZE}em !important;
    font-size: 0.85em;
    text-align: ${(ref) => ref.props.alignment || "left"};
    opacity: ${(ref) => (!isBoolean(ref.props.visible) || ref.props.visible ? 1 : 0)};
    transition: ${(ref) => "color 0.15s ease-in-out," + (ref.props.visible ? "opacity 1s ease-out 0.5s" : "opacity 0.3s ease-out")};
    pointer-events: ${(ref) => (!isBoolean(ref.props.visible) || ref.props.visible ? "auto" : "none")};
    user-select: none;

    > a {
        text-decoration: none !important;
        transition: color 0.5s;

        &:hover {
            color: ${(ref) => ref.props.styles.font.color} !important;
            text-decoration: none !important;
        }
    }
`;

export const Banner = (props: {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
    readonly alignment: "left" | "center" | "right";
    readonly visible?: boolean;
}) =>
    !props.styles.noBranding && props.view !== "preview" ? (
        <BannerElement props={props}>
            <a
                href="https://tripetto.com/your-tripetto-experience/?utm_source=tripetto_runner_autoscroll&utm_medium=tripetto_runners&utm_campaign=tripetto_branding&utm_content=form"
                target="_blank"
            >
                {props.l10n.pgettext("runner:autoscroll", "Powered by Tripetto")}
            </a>
        </BannerElement>
    ) : (
        <></>
    );
