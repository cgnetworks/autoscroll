import { TRunnerViews } from "tripetto-runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "tripetto-runner-foundation";
import { Block, Blocks } from "@ui/blocks";
import { BlockTitle } from "@ui/blocks/title";
import { BlockDescription } from "@ui/blocks/description";

export const PausedMessage = (props: {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
    readonly isPage: boolean;
}) => (
    <Blocks props={{ ...props, isMessage: true, center: props.isPage }}>
        <Block styles={props.styles} view={props.view} isPage={props.isPage} isMessage={true}>
            <BlockTitle props={{ alignment: props.isPage ? "center" : "left" }}>
                {props.l10n.pgettext("runner#2|💬 Messages|Conversation paused", "⏸ Paused")}
            </BlockTitle>
            <BlockDescription props={{ alignment: props.isPage ? "center" : "left" }}>
                {props.l10n.pgettext("runner#2|💬 Messages|Conversation paused", "The conversation is now paused.")}
            </BlockDescription>
        </Block>
    </Blocks>
);
