/** Contains the maximum content width in page mode. */
export const MAX_WIDTH = 1140;

/** This offset is used in inline mode to preserve space for focus borders/shadows. */
export const OFFSET = 5;

/** Margin around the content in full page mode. */
export const MARGIN = 32;

/** Contains the font size. */
export const SIZE = 16;

/** Contains the font size for small screen. */
export const SMALL_SIZE = 14;

/** Number of pixels to indicate a small screen. */
export const SMALL_SCREEN_SIZE = 500;

/** Margin around the content in small screen mode. */
export const SMALL_SCREEN_MARGIN = 16;

/** Contains the navigation height. */
export const NAVIGATION_HEIGHT = 80;
