import styled, { StyleSheetManager, createGlobalStyle, css } from "styled-components";
import { CSSProperties, MutableRefObject, ReactNode, useEffect, useRef, useState } from "react";
import { DateTime, Num } from "tripetto-runner-foundation";
import { TRunnerViews } from "tripetto-runner-react-hook";
import { color } from "tripetto-runner-fabric/color";
import { IViewport } from "@interfaces/viewport";
import { IRuntimeStyles } from "@hooks/styles";
import { Frame } from "@hooks/frame";
import { attach, detach } from "@helpers/resizer";
import { INavigation, Navigation, NavigationElement } from "@ui/navigation";
import { MARGIN, MAX_WIDTH, NAVIGATION_HEIGHT, OFFSET, SMALL_SCREEN_SIZE } from "@ui/const";

const RootFrame = styled(Frame)<{
    props: {
        view: TRunnerViews;
        isPage: boolean;
        styles: IRuntimeStyles;
    };
}>`
    background-color: transparent !important;
    border: none !important;
    position: ${(ref) => (ref.props.view === "live" && ref.props.isPage ? "fixed" : "relative")};
    left: ${(ref) =>
        ref.props.view === "live"
            ? ref.props.isPage || ref.props.styles.direction === "horizontal"
                ? 0
                : `-${ref.props.styles.showEnumerators ? MARGIN : OFFSET}px`
            : undefined};
    top: ${(ref) => (ref.props.view === "live" ? 0 : undefined)};
    width: ${(ref) =>
        ref.props.view === "live" && !ref.props.isPage && ref.props.styles.direction === "vertical"
            ? `calc(100% + ${(ref.props.styles.showEnumerators ? MARGIN : OFFSET) + (ref.props.styles.showScrollbar ? 0 : OFFSET)}px);`
            : "100%"};
    height: ${(ref) => (ref.props.view === "live" && !ref.props.isPage ? 0 : "100%")};
    margin-right: ${(ref) =>
        (ref.props.view === "live" &&
            !ref.props.isPage &&
            ref.props.styles.direction === "vertical" &&
            `-${(ref.props.styles.showEnumerators ? MARGIN : OFFSET) + (ref.props.styles.showScrollbar ? 0 : OFFSET)}px`) ||
        undefined};

    @media (${(ref) => (ref.props.view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        left: ${(ref) =>
            ref.props.view === "live" ? (ref.props.isPage || ref.props.styles.direction === "horizontal" ? 0 : `-${OFFSET}px`) : undefined};
        width: ${(ref) =>
            ref.props.view === "live" && !ref.props.isPage && ref.props.styles.direction === "vertical"
                ? `calc(100% + ${ref.props.styles.showScrollbar ? OFFSET : 0}px);`
                : "100%"};
        margin-right: ${(ref) =>
            (ref.props.view === "live" &&
                !ref.props.isPage &&
                ref.props.styles.direction === "vertical" &&
                `-${OFFSET + (ref.props.styles.showScrollbar ? 0 : OFFSET)}px`) ||
            undefined};
    }
`;

const RootBody = createGlobalStyle<{
    props: {
        customCSS?: string;
    };
}>`
    body {
        overflow: hidden;
        background-color: transparent;

        ${(ref) =>
            ref.props.customCSS &&
            css`
                ${ref.props.customCSS}
            `}
    }
`;

const BackgroundElement = styled.div<{
    props: {
        view: TRunnerViews;
        isPage: boolean;
        styles: IRuntimeStyles;
    };
}>`
    position: absolute;
    left: ${(ref) =>
        ref.props.view === "live" && !ref.props.isPage && ref.props.styles.direction === "vertical"
            ? `${ref.props.styles.showEnumerators ? MARGIN : OFFSET}px`
            : 0};
    right: ${(ref) =>
        ref.props.view === "live" && !ref.props.isPage && ref.props.styles.direction === "vertical" && !ref.props.styles.showScrollbar
            ? `${OFFSET}px`
            : 0};
    top: 0;
    bottom: 0;
    background-color: ${(ref) => color(ref.props.styles.background.color)};
    background-image: ${(ref) =>
        (ref.props.styles.background.url &&
            ref.props.styles.background.opacity > 0 &&
            `${
                (ref.props.styles.background.opacity < 1 &&
                    `linear-gradient(${color(ref.props.styles.background.color, (o) =>
                        o.manipulate((m) => m.alpha(1 - ref.props.styles.background.opacity))
                    )},${color(ref.props.styles.background.color, (o) =>
                        o.manipulate((m) => m.alpha(1 - ref.props.styles.background.opacity))
                    )}),`) ||
                ""
            }url("${ref.props.styles.background.url}")`) ||
        undefined};
    background-size: ${(ref) =>
        (ref.props.styles.background.url &&
            ref.props.styles.background.positioning !== "repeat" &&
            ref.props.styles.background.positioning) ||
        undefined};
    background-repeat: ${(ref) =>
        ref.props.styles.background.url && ref.props.styles.background.positioning === "repeat" ? "repeat" : "no-repeat"};
    background-position: center center;
    transition: background 0.15s ease-in-out;
`;

const RootElement = styled.div<{
    props: {
        view: TRunnerViews;
        isPage: boolean;
        autoHeight: boolean;
        disableScrolling: boolean;
        styles: IRuntimeStyles;
        fontFamily: string;
        navigation?: INavigation;
    };
}>`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: ${(ref) => (ref.props.navigation && `${NAVIGATION_HEIGHT}px`) || 0};
    font-family: ${(ref) => ref.props.fontFamily};
    font-size: ${(ref) => ref.props.styles.font.size}px;
    font-variant-ligatures: none;
    line-height: 1.5em;
    outline: 0;
    overflow-x: ${(ref) => (ref.props.styles.direction === "horizontal" ? "auto" : "hidden")};
    overflow-y: ${(ref) => (ref.props.autoHeight || ref.props.disableScrolling ? "hidden" : "auto")};
    touch-action: ${(ref) => (ref.props.styles.disableScrolling ? "none" : "pan-y")};
    scroll-behavior: smooth;
    scrollbar-width: ${(ref) => (!ref.props.styles.showScrollbar && ref.props.view !== "preview" ? "none" : undefined)};
    -ms-overflow-style: ${(ref) => (!ref.props.styles.showScrollbar && ref.props.view !== "preview" ? "none" : undefined)};
    -webkit-overflow-scrolling: touch;
    -webkit-tap-highlight-color: transparent;

    ::-webkit-scrollbar {
        display: ${(ref) => (!ref.props.styles.showScrollbar && ref.props.view !== "preview" ? "none" : undefined)};
    }

    > div {
        max-width: ${(ref) => ref.props.styles.direction === "vertical" && ref.props.isPage && `${MAX_WIDTH}px`};
        min-height: ${(ref) => (ref.props.view !== "live" || ref.props.isPage ? "100%" : undefined)};
        margin-left: ${(ref) => ref.props.styles.direction === "vertical" && ref.props.isPage && "auto"};
        margin-right: ${(ref) => ref.props.styles.direction === "vertical" && ref.props.isPage && "auto"};
        display: flex;
    }

    * {
        font-family: ${(ref) => ref.props.fontFamily};
        font-variant-ligatures: none;
        box-sizing: border-box;
        outline: none;
    }

    a {
        text-decoration: underline;
        color: inherit !important;
    }

    ${NavigationElement} {
        left: ${(ref) =>
            ref.props.view === "live" && !ref.props.isPage && ref.props.styles.direction === "vertical"
                ? `${ref.props.styles.showEnumerators ? MARGIN : OFFSET}px`
                : undefined};
        right: ${(ref) =>
            ref.props.view === "live" && !ref.props.isPage && ref.props.styles.direction === "vertical" && !ref.props.styles.showScrollbar
                ? `${OFFSET}px`
                : undefined};
    }

    @media (prefers-reduced-motion: reduce) {
        scroll-behavior: auto;
    }

    @media (${(ref) => (ref.props.view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        font-size: ${(ref) => ref.props.styles.font.sizeSmall}px;
    }
`;

const Content = (props: { readonly children?: ReactNode; readonly onChange: (height: number, force: boolean) => void }) => {
    const contentRef = useRef<HTMLElement>() as MutableRefObject<HTMLDivElement>;

    useEffect(() => {
        const ref = attach((force) => {
            if (contentRef.current) {
                props.onChange(contentRef.current.getBoundingClientRect().height, force);
            }
        });

        return () => detach(ref);
    });

    return <div ref={contentRef}>{props.children}</div>;
};

export const AutoscrollRoot = (props: {
    readonly frameRef: MutableRefObject<HTMLIFrameElement>;
    readonly view: TRunnerViews;
    readonly isPage: boolean;
    readonly height?: number | "auto";
    readonly disableScrolling: boolean;
    readonly onResize: () => void;
    readonly onScroll: (rect: IViewport) => void;
    readonly onKey: (action: "next" | "previous") => void;
    readonly onTouch?: () => void;
    readonly navigation?: INavigation;
    readonly title?: string;
    readonly styles: IRuntimeStyles;
    readonly className?: string;
    readonly customStyle?: CSSProperties;
    readonly customCSS?: string;
    readonly children?: ReactNode;
}) => {
    const [contentHeight, setContentHeight] = useState(0);
    const rootRef = useRef<HTMLDivElement>() as MutableRefObject<HTMLDivElement | null>;
    const scrollbarSize = useRef(0);
    const scrollRef = useRef<() => void>();
    const scrollSmoothSupport = useRef<boolean>();
    const scrollAnimationRef = useRef<() => void>();
    const onScroll = (scrollRef.current = () => {
        const scrollRect = rootRef.current?.getBoundingClientRect();

        props.onScroll({
            timestamp: DateTime.precise,
            left: scrollRect?.left || 0,
            top: scrollRect?.top || 0,
            width: scrollRect?.width || 0,
            height: scrollRect?.height || 0,
            scrollLeft: rootRef.current?.scrollLeft || 0,
            scrollTop: rootRef.current?.scrollTop || 0,
            scrollTo: (left: number, top: number, smooth: boolean = true) => {
                if (rootRef.current) {
                    top = top > 0 ? Num.round(top) : 0;
                    left = left > 0 ? Num.round(left) : 0;

                    rootRef.current.style.scrollBehavior = smooth ? "" : "auto";

                    if (scrollSmoothSupport.current === false || !rootRef.current.scrollTo) {
                        if (scrollAnimationRef.current) {
                            scrollAnimationRef.current();
                        }

                        if (smooth) {
                            const element = rootRef.current;
                            const start = DateTime.precise;
                            const offsetX = ~~element.scrollLeft;
                            const offsetY = ~~element.scrollTop;
                            const deltaX = ~~(left - offsetX);
                            const deltaY = ~~(top - offsetY);
                            const duration = 400;
                            const animate = () => {
                                const requestId = requestAnimationFrame(() => {
                                    const t = Num.range(DateTime.precise - start, 0, duration);
                                    const f = t / duration;
                                    const x = -deltaX * f * (f - 2);
                                    const y = -deltaY * f * (f - 2);

                                    element.scrollLeft = offsetX + x;
                                    element.scrollTop = offsetY + y;

                                    if (t !== duration) {
                                        animate();
                                    } else {
                                        element.scrollLeft = left;
                                        element.scrollTop = top;
                                    }
                                });

                                scrollAnimationRef.current = () => cancelAnimationFrame(requestId);
                            };

                            animate();
                        } else {
                            rootRef.current.scrollLeft = left;
                            rootRef.current.scrollTop = top;
                        }
                    } else {
                        rootRef.current.scrollTo({
                            left,
                            top,
                            behavior: smooth ? "smooth" : "auto",
                        });
                    }
                }
            },
        });
    });

    useEffect(() => {
        if (props.frameRef.current && props.view === "live" && (props.isPage || props.styles.autoFocus)) {
            props.frameRef.current.focus();
        }
    }, [contentHeight]);

    useEffect(() => {
        if (typeof scrollSmoothSupport.current !== "boolean" && rootRef.current) {
            scrollSmoothSupport.current = getComputedStyle(rootRef.current).scrollBehavior === "smooth";
        }

        if (
            !scrollbarSize.current &&
            props.styles.direction === "horizontal" &&
            (props.styles.showScrollbar || props.view === "preview") &&
            rootRef.current &&
            rootRef.current.clientHeight > 0 &&
            rootRef.current.offsetHeight > rootRef.current.clientHeight
        ) {
            scrollbarSize.current = Num.max(rootRef.current.offsetHeight - rootRef.current.clientHeight, 0);

            setContentHeight(contentHeight + scrollbarSize.current);
        }

        onScroll();

        if (rootRef.current && props.styles.disableScrolling) {
            const wheelElement = rootRef.current;
            const wheelListener = (wheelEvent: WheelEvent) => {
                wheelEvent.preventDefault();
                wheelEvent.stopPropagation();

                return false;
            };

            wheelElement.addEventListener("wheel", wheelListener, {
                passive: false,
            });

            return () => {
                wheelElement.removeEventListener("wheel", wheelListener);
            };
        }

        return;
    });

    return (
        <RootFrame
            props={props}
            frameRef={props.frameRef}
            resizeRef={scrollRef}
            title={props.title}
            style={{
                ...props.customStyle,
                height:
                    props.customStyle && props.customStyle.height && props.customStyle.height !== "auto"
                        ? props.customStyle.height
                        : props.height === "auto"
                        ? contentHeight + (props.navigation ? NAVIGATION_HEIGHT : 0)
                        : props.height,
            }}
            font={props.styles.font && props.styles.font.family}
            className={props.className}
            onTouch={props.onTouch}
        >
            {(doc: Document, fontFamily: string) => (
                <StyleSheetManager target={doc.head}>
                    <>
                        <RootBody props={props} />
                        <BackgroundElement props={props} />
                        <RootElement
                            ref={rootRef}
                            props={{
                                ...props,
                                fontFamily,
                                autoHeight: props.height === "auto",
                            }}
                            onScroll={onScroll}
                            tabIndex={0}
                            onKeyDown={(e: React.KeyboardEvent<HTMLDivElement>) => {
                                if (e.key === "PageUp") {
                                    e.preventDefault();

                                    props.onKey("previous");
                                }

                                if (e.key === "PageDown") {
                                    e.preventDefault();

                                    props.onKey("next");
                                }

                                if (doc.activeElement?.tagName === "INPUT" || doc.activeElement?.tagName === "TEXTAREA") {
                                    return;
                                }

                                if (
                                    (e.key === "ArrowUp" && props.styles.direction === "vertical") ||
                                    (e.key === "ArrowLeft" && props.styles.direction === "horizontal")
                                ) {
                                    e.preventDefault();

                                    props.onKey("previous");
                                } else if (
                                    (e.key === "ArrowDown" && props.styles.direction === "vertical") ||
                                    (e.key === "ArrowRight" && props.styles.direction === "horizontal")
                                ) {
                                    e.preventDefault();

                                    props.onKey("next");
                                } else if (e.key === " " && doc.activeElement?.tagName !== "BUTTON") {
                                    e.preventDefault();

                                    if (e.shiftKey) {
                                        props.onKey("previous");
                                    } else {
                                        props.onKey("next");
                                    }
                                }
                            }}
                        >
                            <Content
                                onChange={(height, force) => {
                                    height =
                                        Num.ceil(height) +
                                        ((props.styles.direction === "horizontal" &&
                                            (props.styles.showScrollbar || props.view === "preview") &&
                                            scrollbarSize.current) ||
                                            0);

                                    if (height !== contentHeight || force) {
                                        setContentHeight(height);

                                        props.onResize();
                                    }
                                }}
                            >
                                {props.children}
                            </Content>
                        </RootElement>
                        {props.navigation && <Navigation {...props.navigation} isPage={props.isPage} fontFamily={fontFamily} />}
                    </>
                </StyleSheetManager>
            )}
        </RootFrame>
    );
};
