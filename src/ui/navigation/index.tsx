import styled from "styled-components";
import { useRef } from "react";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "tripetto-runner-foundation";
import { TRunnerViews } from "tripetto-runner-react-hook";
import { ButtonFabric } from "tripetto-runner-fabric/components/button";
import { LeftIcon } from "@ui/icons/left";
import { RightIcon } from "@ui/icons/right";
import { UpIcon } from "@ui/icons/up";
import { DownIcon } from "@ui/icons/down";
import { PauseIcon } from "@ui/icons/pause";
import { Progressbar } from "./progressbar";
import { MARGIN, MAX_WIDTH, NAVIGATION_HEIGHT, OFFSET, SMALL_SCREEN_SIZE } from "@ui/const";

export const NavigationElement = styled.nav<{
    props: {
        styles: IRuntimeStyles;
        view: TRunnerViews;
        isPage: boolean;
        fontFamily: string;
    };
}>`
    position: fixed;
    left: ${(ref) =>
        ref.props.isPage || ref.props.styles.direction === "horizontal" ? 0 : `${ref.props.styles.showEnumerators ? MARGIN : OFFSET}px`};
    bottom: 0;
    right: ${(ref) =>
        ref.props.isPage || ref.props.styles.direction === "horizontal" ? 0 : `${ref.props.styles.showScrollbar ? 0 : OFFSET}px`};
    background-color: ${(ref) => ref.props.styles.navigation?.backgroundColor};
    font-family: ${(ref) => ref.props.fontFamily};
    font-size: 16px;
    font-variant-ligatures: none;
    transition: background-color 0.15s ease-in-out;
    user-select: none;

    > div {
        width: 100%;
        height: ${NAVIGATION_HEIGHT}px;
        max-width: ${(ref) => ref.props.isPage && `${MAX_WIDTH - MARGIN * 2 + 32}px`};
        margin-left: ${(ref) => ref.props.isPage && "auto"};
        margin-right: ${(ref) => ref.props.isPage && "auto"};
        display: flex;
        justify-content: space-between;
        align-items: center;

        > div:first-child {
            width: 50%;
            padding: 16px;

            @media (${(ref) => (ref.props.view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
                flex-grow: 2;
            }

            > div {
                font-size: 12px;
                line-height: 16px;
                color: ${(ref) => ref.props.styles.navigation?.textColor};
                transition: color 0.15s ease-in-out;

                &:first-child {
                    font-weight: bold;
                    overflow: hidden;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                }

                &:last-child {
                    font-weight: normal;

                    > a {
                        text-decoration: none;
                        color: inherit !important;

                        &:hover {
                            text-decoration: underline;
                        }
                    }
                }
            }
        }

        > div:last-child {
            padding-right: 8px;
            white-space: nowrap;

            > * {
                margin-right: 8px;
            }
        }
    }
`;

export interface INavigation {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly total: number;
    readonly progress: number;
    readonly answerable: number;
    readonly answered: number;
    readonly pausable: boolean;
    readonly isPausing: boolean;
    readonly isEvaluating: boolean;
    readonly isFinishing: boolean;
    readonly view: TRunnerViews;
    readonly previous?: () => void;
    readonly next?: () => void;
    readonly pause?: () => void;
}

export const Navigation = (
    props: INavigation & {
        readonly isPage: boolean;
        readonly fontFamily: string;
    }
) => {
    const progressRef = useRef(0);
    const buttonStyles = {
        baseColor: props.styles.navigation?.textColor || "black",
        mode: "outline" as "outline",
        roundness: props.styles.buttons.roundness,
        scale: 2,
    };

    if (!props.isPausing && !props.isEvaluating) {
        progressRef.current = props.progress;
    }

    return (
        <NavigationElement props={props}>
            <div>
                <div>
                    {props.styles.showProgressbar && (
                        <>
                            <div>
                                {props.isFinishing
                                    ? props.l10n.pgettext("runner#3|🩺 Status information", "Submitting...")
                                    : props.isEvaluating
                                    ? props.l10n.pgettext("runner#3|🩺 Status information", "⏳ One moment please...")
                                    : props.isPausing
                                    ? props.l10n.pgettext("runner#3|🩺 Status information", "Pausing...")
                                    : props.view === "preview"
                                    ? props.l10n.pgettext("runner:autoscroll", "Preview mode (shows all without logic)")
                                    : props.answerable === 0
                                    ? props.l10n.pgettext("runner#3|🩺 Status information", "Nothing to answer")
                                    : props.l10n.pgettext(
                                          "runner#3|🩺 Status information",
                                          "%1 of %2 answered",
                                          `${props.answered}`,
                                          `${props.answerable}`
                                      )}
                            </div>
                            <Progressbar
                                total={props.total}
                                progress={
                                    props.isFinishing
                                        ? props.total
                                        : props.isPausing || props.isEvaluating
                                        ? progressRef.current
                                        : props.progress
                                }
                                color={props.styles.navigation?.progressbarColor || "black"}
                            />
                        </>
                    )}
                    {!props.styles.noBranding && props.view !== "preview" && (
                        <div>
                            <a
                                href="https://tripetto.com/your-tripetto-experience/?utm_source=tripetto_runner_autoscroll&utm_medium=tripetto_runners&utm_campaign=tripetto_branding&utm_content=form"
                                target="_blank"
                            >
                                {props.l10n.pgettext("runner:autoscroll", "Powered by Tripetto")}
                            </a>
                        </div>
                    )}
                </div>
                <div>
                    <ButtonFabric
                        styles={buttonStyles}
                        icon={props.styles.direction === "horizontal" ? LeftIcon : UpIcon}
                        onClick={props.previous}
                    />
                    {props.pausable && <ButtonFabric styles={buttonStyles} icon={PauseIcon} onClick={props.pause} />}
                    <ButtonFabric
                        styles={buttonStyles}
                        icon={props.styles.direction === "horizontal" ? RightIcon : DownIcon}
                        onClick={props.next}
                    />
                </div>
            </div>
        </NavigationElement>
    );
};
