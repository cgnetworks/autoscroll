import styled from "styled-components";

export const BlockExplanation = styled.small`
    display: block;
    margin: 0;
    padding: 0;
    font-size: 0.8em;
    user-select: none;
    cursor: default;
`;
