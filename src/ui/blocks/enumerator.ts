import styled from "styled-components";
import { TRunnerViews } from "tripetto-runner-react-hook";
import { SMALL_SCREEN_SIZE } from "@ui/const";

export const BlockEnumerator = styled.span<{
    props: {
        view: TRunnerViews;
    };
}>`
    font-size: 14px;
    font-weight: normal;
    line-height: 1em;
    display: inline-block;
    width: 32px;
    margin-left: -32px;
    text-align: center;

    @media (${(ref) => (ref.props.view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        display: none;
    }
`;
