import styled from "styled-components";
import { useLoader } from "@helpers/resizer";

const ImageContainer = styled.div<{
    props: {
        aligment: "left" | "center" | "right";
    };
}>`
    width: 100%;
    display: flex;
    align-items: flex-start;
    justify-content: ${(ref) => ref.props.aligment};
`;

const ImageElement = styled.img<{
    props: {
        isPage: boolean;
    };
}>`
    margin: 0;
    padding: 0;
    border: none;
    max-width: 100%;
    height: auto;
    border-style: none;
`;

export const BlockImage = (props: {
    readonly src: string;
    readonly isPage: boolean;
    readonly width?: string;
    readonly alignment?: "left" | "center" | "right";
    readonly onClick?: () => void;
}) => {
    const loader = useLoader(props.src);

    return (
        <ImageContainer
            props={{
                aligment: props.alignment || "left",
            }}
            onClick={props.onClick}
        >
            <ImageElement
                props={props}
                src={props.src}
                style={{
                    width: props.width,
                }}
                onLoad={loader}
                onError={loader}
            />
        </ImageContainer>
    );
};
