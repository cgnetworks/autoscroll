import { L10n, TStatus, noop } from "tripetto-runner-foundation";
import { TRunnerStatus, TRunnerViews } from "tripetto-runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { TAutoFocus } from "@hooks/focus";
import { TElement } from "@interfaces/element";
import { Block } from ".";
import { BlockButtons } from "./buttons";
import { ButtonFabric } from "tripetto-runner-fabric/components/button";
import { SubmitIcon } from "@ui/icons/submit";
import { PreviewMessage } from "@ui/messages/preview";

export const BLOCK_SUBMIT = "submit";

export const SubmitBlock = (props: {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
    readonly isPage: boolean;
    readonly status: TRunnerStatus | TStatus;
    readonly active: string | undefined;
    readonly width: number | undefined;
    readonly offset: number | undefined;
    readonly onElement: TElement;
    readonly onActivate: () => void;
    readonly onSubmit: (() => void) | undefined;
    readonly onPrevious: () => void;
    readonly onAutoFocus: TAutoFocus;
    readonly onTab: () => void;
}) => {
    const isActivated = props.active === BLOCK_SUBMIT;

    return (
        <Block
            key={BLOCK_SUBMIT}
            element={props.onElement(BLOCK_SUBMIT)}
            l10n={props.l10n}
            styles={props.styles}
            view={props.view}
            isPage={props.isPage}
            isActivated={isActivated}
            isHidden={props.styles.hideUpcoming && !isActivated}
            width={props.width}
            top={props.offset}
            onActivate={props.onActivate}
            onPrevious={props.onPrevious}
        >
            <BlockButtons styles={props.styles} isActivated={true}>
                <ButtonFabric
                    styles={props.styles.finishButton}
                    disabled={props.status === "finishing" || props.status === "pausing" || props.status === "evaluating"}
                    icon={SubmitIcon}
                    label={
                        props.status === "finishing"
                            ? props.l10n.pgettext("runner#3|🩺 Status information", "Submitting...")
                            : props.l10n.pgettext("runner#1|🆗 Buttons", "Submit")
                    }
                    onClick={props.view === "preview" ? () => noop() : props.onSubmit}
                    onAutoFocus={props.onAutoFocus(
                        {
                            key: BLOCK_SUBMIT,
                        },
                        isActivated
                    )}
                    onTab={props.onTab}
                    onCancel={props.onPrevious}
                />
                {props.view === "preview" && <PreviewMessage l10n={props.l10n} styles={props.styles} />}
            </BlockButtons>
        </Block>
    );
};
