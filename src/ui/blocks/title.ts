import styled from "styled-components";

export const BlockTitle = styled.h2<{
    props?: {
        hasDescription?: boolean;
        alignment?: "left" | "center" | "right";
    };
}>`
    display: block;
    margin: 0;
    padding: 0;
    font-weight: bold;
    font-size: 2em;
    line-height: 1.2em;
    text-align: ${(ref) => (ref.props && ref.props.alignment) || "left"};
    user-select: none;
`;
