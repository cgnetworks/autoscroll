import styled from "styled-components";

export const BlockDescription = styled.p<{
    props?: {
        alignment?: "left" | "center" | "right";
    };
}>`
    margin: 0;
    padding: 0;
    font-size: 1em;
    text-align: ${(ref) => (ref.props && ref.props.alignment) || "left"};
    user-select: none;
`;
