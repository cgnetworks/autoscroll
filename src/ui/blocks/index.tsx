import styled from "styled-components";
import { ReactNode } from "react";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n, castToBoolean } from "tripetto-runner-foundation";
import { TRunnerViews } from "tripetto-runner-react-hook";
import { BlockPreviousButton } from "@ui/blocks/previous";
import { BannerElement } from "@ui/messages/banner";
import { Banner } from "@ui/messages/banner";
import { MARGIN, MAX_WIDTH, OFFSET, SIZE, SMALL_SCREEN_MARGIN, SMALL_SCREEN_SIZE } from "@ui/const";

export const Blocks = styled.div<{
    props: {
        styles: IRuntimeStyles;
        view?: TRunnerViews;
        center?: boolean;
        isPage?: boolean;
        isMessage?: boolean;
        isCalculated?: boolean;
        size?: number;
    };
}>`
    display: ${(ref) => (!ref.props.isMessage && ref.props.styles.direction === "horizontal" ? "flex" : "block")};
    min-width: 100%;
    flex-direction: ${(ref) => !ref.props.isMessage && ref.props.styles.direction === "horizontal" && "row"};
    flex-wrap: ${(ref) => !ref.props.isMessage && ref.props.styles.direction === "horizontal" && "nowrap"};
    align-items: ${(ref) => !ref.props.isMessage && ref.props.styles.direction === "horizontal" && "start"};
    align-self: ${(ref) => ref.props.center && "center"};
    opacity: ${(ref) => (castToBoolean(ref.props.isCalculated, true) ? 1 : 0)};
    font-size: ${(ref) => (ref.props.size && `${ref.props.size}px`) || undefined};
    line-height: ${(ref) => (ref.props.size && "1.5em") || undefined};

    > div {
        color: ${(ref) => ref.props.styles.font.color};
        flex: ${(ref) => !ref.props.isMessage && ref.props.styles.direction === "horizontal" && "0 0 calc(100vw)"};
        transition: color 0.15s ease-in-out;
    }
`;

export const BlockElement = styled.div<{
    props: {
        view: TRunnerViews;
        styles: IRuntimeStyles;
        isPage: boolean;
        isActivated?: boolean;
        isHidden?: boolean;
        isLocked?: boolean;
        isMessage?: boolean;
        hasBanner?: boolean;
    };
}>`
    display: block;
    position: relative;
    contain: content;
    opacity: ${(ref) => (ref.props.isMessage || (ref.props.isActivated && !ref.props.isLocked) ? 1 : ref.props.isHidden ? 0 : 0.2)};
    pointer-events: ${(ref) => (ref.props.isMessage || (ref.props.isActivated && !ref.props.isLocked) ? "auto" : "none")};
    transition: opacity 0.5s, color 0.15s ease-in-out !important;
    user-select: ${(ref) => (!ref.props.isMessage && ref.props.isActivated ? "auto" : "none")};
    padding-left: ${(ref) => `${ref.props.isPage || ref.props.view !== "live" || ref.props.styles.showEnumerators ? MARGIN : OFFSET}px`};
    padding-right: ${(ref) => `${ref.props.isPage || ref.props.view !== "live" || ref.props.styles.showScrollbar ? MARGIN : OFFSET}px`};
    padding-top: ${(ref) => `${MARGIN + (ref.props.isPage || ref.props.view !== "live" ? 8 : 0)}px`};
    padding-bottom: ${(ref) => `${MARGIN + (ref.props.isPage || ref.props.view !== "live" ? 8 : 0)}px`};
    z-index: 1;

    @media (${(ref) => (ref.props.view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        padding-left: ${(ref) =>
            `${ref.props.isPage || ref.props.view !== "live" || ref.props.styles.showEnumerators ? SMALL_SCREEN_MARGIN : OFFSET}px`};
        padding-right: ${(ref) =>
            `${ref.props.isPage || ref.props.view !== "live" || ref.props.styles.showScrollbar ? SMALL_SCREEN_MARGIN : OFFSET}px`};
    }

    &:first-child {
        padding-top: ${(ref) => !ref.props.isPage && ref.props.view === "live" && `${OFFSET}px`};
    }

    &:last-child {
        padding-bottom: ${(ref) =>
            !ref.props.isPage &&
            ref.props.view === "live" &&
            !ref.props.hasBanner &&
            (ref.props.isMessage || !ref.props.styles.navigation) &&
            `${OFFSET}px`};
    }

    > div:first-child {
        max-width: ${(ref) => ref.props.styles.direction === "horizontal" && ref.props.isPage && `${MAX_WIDTH - MARGIN * 2}px`};

        > * {
            margin-top: ${16 / SIZE}em !important;

            &:first-child {
                margin-top: 0 !important;
            }
        }

        > h2 + p {
            margin-top: ${8 / SIZE}em !important;
        }

        > h2 + h3 {
            margin-top: ${8 / SIZE}em !important;
        }

        > h3 + p {
            margin-top: ${8 / SIZE}em !important;
        }
    }

    > div:nth-child(2) {
        max-width: ${(ref) => ref.props.styles.direction === "horizontal" && ref.props.isPage && `${MAX_WIDTH}px`};
    }

    > div {
        margin-left: ${(ref) => ref.props.styles.direction === "horizontal" && ref.props.isPage && "auto"};
        margin-right: ${(ref) => ref.props.styles.direction === "horizontal" && ref.props.isPage && "auto"};
    }
`;

const BlockSurfaceElement = styled.div<{
    props: {
        view: TRunnerViews;
        styles: IRuntimeStyles;
        isPage: boolean;
        isActivated?: boolean;
    };
}>`
    position: absolute;
    left: ${(ref) => (ref.props.view === "live" && !ref.props.isPage ? `${ref.props.styles.showEnumerators ? MARGIN : OFFSET}px` : 0)};
    top: 0;
    right: 0;
    bottom: 0;
    pointer-events: ${(ref) => (ref.props.isActivated ? "none" : "auto")};
    cursor: default;

    ${BannerElement} {
        position: absolute;
        bottom: ${(ref) => (ref.props.isPage || ref.props.view !== "live" ? `8px` : 0)};
        left: ${(ref) => (ref.props.isPage || ref.props.view !== "live" ? `${MARGIN}px` : undefined)};

        > a {
            position: relative;
            top: 0.1em;
        }

        @media (${(ref) => (ref.props.view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
            left: ${(ref) => (ref.props.isPage || ref.props.view !== "live" ? `${SMALL_SCREEN_MARGIN}px` : undefined)};
        }
    }
`;

export const Block = (props: {
    readonly identifier?: string;
    readonly element?: (ref: HTMLElement | null) => void;
    readonly l10n?: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
    readonly isPage: boolean;
    readonly isActivated?: boolean;
    readonly isHidden?: boolean;
    readonly isLocked?: boolean;
    readonly isMessage?: boolean;
    readonly width?: number;
    readonly top?: number;
    readonly showBanner?: boolean;
    readonly onActivate?: () => void;
    readonly onPrevious?: () => void;
    readonly children?: ReactNode;
}) => (
    <BlockElement
        ref={props.element}
        data-block={props.identifier}
        props={{
            ...props,
            hasBanner: props.l10n && props.onActivate && !props.styles.navigation && !props.styles.noBranding && props.view !== "preview",
        }}
        style={{
            width: props.width,
            top: props.top,
        }}
    >
        <div>{props.children}</div>
        {props.l10n && props.onActivate && (
            <BlockSurfaceElement props={props} onClick={(!props.isActivated && !props.isHidden && props.onActivate) || undefined}>
                {props.styles.showPreviousButton && props.onPrevious && (
                    <BlockPreviousButton
                        l10n={props.l10n}
                        styles={props.styles}
                        view={props.view}
                        isPage={props.isPage}
                        isActivated={props.isActivated || false}
                        onClick={props.onPrevious}
                    />
                )}
                {!props.styles.navigation && (
                    <Banner l10n={props.l10n} styles={props.styles} view={props.view} alignment="left" visible={props.showBanner} />
                )}
            </BlockSurfaceElement>
        )}
    </BlockElement>
);
