import styled from "styled-components";
import { L10n } from "tripetto-runner-foundation";
import { TRunnerViews } from "tripetto-runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { MARGIN, SMALL_SCREEN_MARGIN, SMALL_SCREEN_SIZE } from "@ui/const";

const PreviousButtonElement = styled.div<{
    props: {
        styles: IRuntimeStyles;
        view: TRunnerViews;
        isPage: boolean;
        isActivated: boolean;
    };
}>`
    position: absolute;
    left: ${(ref) => (ref.props.isPage || ref.props.view !== "live" ? `${MARGIN}px` : 0)};
    top: ${(ref) => (ref.props.isPage || ref.props.view !== "live" ? `8px` : 0)};
    height: ${MARGIN}px;
    transform: ${(ref) => !ref.props.isActivated && `translate${ref.props.styles.direction === "horizontal" ? "X" : "Y"}(16px)`};
    opacity: ${(ref) => (ref.props.isActivated ? 0.6 : 0)};
    pointer-events: ${(ref) => (ref.props.isActivated ? "auto" : "none")};
    transition: opacity 0.3s ease-out 0.2s, transform 0.3s ease-out 0.2s;
    cursor: pointer;
    font-size: 12px;
    user-select: none;

    @media (${(ref) => (ref.props.view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        left: ${(ref) => (ref.props.isPage || ref.props.view !== "live" ? `${SMALL_SCREEN_MARGIN}px` : 0)};
    }

    > svg {
        width: 16px;
        height: 16px;
        position: relative;
        top: ${(ref) => (ref.props.styles.direction === "horizontal" ? 3 : 3)}px;
        margin-right: 4px;
    }

    &:hover {
        opacity: ${(ref) => (ref.props.isActivated ? 1 : 0)};
    }
`;

export const BlockPreviousButton = (props: {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
    readonly isPage: boolean;
    readonly isActivated: boolean;
    readonly offset?: number;
    readonly onClick: () => void;
}) => (
    <PreviousButtonElement props={props} onClick={props.onClick}>
        <svg viewBox="0 0 20 20">
            {props.styles.direction === "horizontal" ? (
                <path
                    fill={props.styles.font.color}
                    d="M0.646 10.146l6-6c0.195-0.195 0.512-0.195 0.707 0s0.195 0.512 0 0.707l-5.146 5.146h16.293c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5h-16.293l5.146 5.146c0.195 0.195 0.195 0.512 0 0.707-0.098 0.098-0.226 0.146-0.354 0.146s-0.256-0.049-0.354-0.146l-6-6c-0.195-0.195-0.195-0.512 0-0.707z"
                />
            ) : (
                <path
                    fill={props.styles.font.color}
                    d="M9.146 0.646l-6 6c-0.195 0.195-0.195 0.512 0 0.707s0.512 0.195 0.707 0l5.146-5.146v16.293c0 0.276 0.224 0.5 0.5 0.5s0.5-0.224 0.5-0.5v-16.293l5.146 5.146c0.195 0.195 0.512 0.195 0.707 0 0.098-0.098 0.146-0.226 0.146-0.354s-0.049-0.256-0.146-0.354l-6-6c-0.195-0.195-0.512-0.195-0.707 0z"
                />
            )}
        </svg>
        {props.l10n.pgettext("runner#1|🆗 Buttons", "Back")}
    </PreviousButtonElement>
);
