import styled from "styled-components";
import { useState } from "react";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "tripetto-runner-foundation";
import { ButtonFabric } from "tripetto-runner-fabric/components/button";
import { color } from "tripetto-runner-fabric/color";
import { RetryIcon } from "@ui/icons/retry";
import { CancelIcon } from "@ui/icons/cancel";
import { SIZE } from "@ui/const";

const Buttons = styled.div`
    display: block;
    margin-top: ${16 / SIZE}em;

    * + * {
        margin-left: ${16 / SIZE}em;
    }
`;

export const OutdatedError = (props: {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly onReload?: () => void;
    readonly onDiscard: () => void;
}) => {
    const [isReloading, setReloading] = useState(false);

    return (
        <>
            <b>{props.l10n.pgettext("runner#9|⚠ Errors|Submit error", "The form is outdated.")}</b>
            <br />
            {props.onReload
                ? props.l10n.pgettext(
                      "runner#9|⚠ Errors|Outdated error",
                      "It seems the form is changed by the owner while you were filling in the form. Your data cannot be processed, but you can try to reload the new version of the form while maintaining the data you've filled in. Click the reload button to try this. Please check your data after reloading and then submit again."
                  )
                : props.l10n.pgettext(
                      "runner#9|⚠ Errors|Outdated error",
                      "It seems the form is changed by the owner while you were filling in the form. Your data cannot be processed. Try reloading the form."
                  )}
            <Buttons>
                {props.onReload && (
                    <ButtonFabric
                        styles={{ baseColor: color(props.styles.inputs.errorColor, (o) => o.makeBlackOrWhite()), mode: "outline" }}
                        label={props.l10n.pgettext("runner#1|🆗 Buttons", "Reload")}
                        icon={RetryIcon}
                        disabled={isReloading}
                        onClick={() => {
                            setReloading(true);

                            props.onReload!();
                        }}
                    />
                )}
                <ButtonFabric
                    styles={{ baseColor: color(props.styles.inputs.errorColor, (o) => o.makeBlackOrWhite()), mode: "outline" }}
                    label={props.l10n.pgettext("runner#1|🆗 Buttons", "Discard")}
                    icon={CancelIcon}
                    onClick={props.onDiscard}
                />
            </Buttons>
        </>
    );
};
