import styled from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "tripetto-runner-foundation";
import { ButtonFabric } from "tripetto-runner-fabric/components/button";
import { color } from "tripetto-runner-fabric/color";
import { CancelIcon } from "@ui/icons/cancel";
import { SIZE } from "@ui/const";

const Buttons = styled.div`
    display: block;
    margin-top: ${16 / SIZE}em;
`;

export const PauseError = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles; readonly onDiscard?: () => void }) => (
    <>
        <b>{props.l10n.pgettext("runner#9|⚠ Errors|Pausing error", "Something went wrong while pausing your conversation.")}</b>
        <br />
        {props.l10n.pgettext(
            "runner#9|⚠ Errors|Connection error",
            "Please check your connection and try again (for the techies: The error console of your browser might contain more technical information about what went wrong)."
        )}
        <Buttons>
            <ButtonFabric
                styles={{ baseColor: color(props.styles.inputs.errorColor, (o) => o.makeBlackOrWhite()), mode: "outline" }}
                label={props.l10n.pgettext("runner:autoscroll", "Discard")}
                icon={CancelIcon}
                onClick={props.onDiscard}
            />
        </Buttons>
    </>
);
