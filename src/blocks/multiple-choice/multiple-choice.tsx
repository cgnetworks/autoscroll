import { ReactNode } from "react";
import { findFirst, tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { MultipleChoice } from "tripetto-block-multiple-choice/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { BlockCaption } from "@ui/blocks/caption";
import { MultipleChoiceFabric } from "tripetto-runner-fabric/components/multiple-choice";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-multiple-choice",
    alias: "multiple-choice",
    autoRender: true,
})
export class MultipleChoiceBlock extends MultipleChoice implements IAutoscrollRendering {
    readonly hideButtons = !this.props.multiple && findFirst(this.props.choices, (choice) => !!choice.name && !choice.url) ? true : false;
    readonly autoSubmit = this.hideButtons;

    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.name}
                {this.props.caption && <BlockCaption>{props.markdownifyToJSX(this.props.caption)}</BlockCaption>}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <MultipleChoiceFabric
                    styles={props.styles.multipleChoice}
                    view={props.view}
                    buttons={this.choices(props)}
                    alignment={(this.props.alignment && "horizontal") || "vertical"}
                    value={(!this.props.multiple && this.valueOf("choice")) || undefined}
                    required={this.required}
                    ariaDescribedBy={props.ariaDescribedBy}
                    autoSubmit={this.autoSubmit}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
