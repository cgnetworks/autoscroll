import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { URL } from "tripetto-block-url/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { URLFabric } from "tripetto-runner-fabric/components/url";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-url",
})
export class URLBlock extends URL implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <URLFabric
                    id={props.id}
                    styles={props.styles.inputs}
                    value={this.urlSlot}
                    required={this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
