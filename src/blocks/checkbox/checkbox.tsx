import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Checkbox } from "tripetto-block-checkbox/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { CheckboxFabric } from "tripetto-runner-fabric/components/checkbox";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-checkbox",
    alias: "checkbox",
})
export class CheckboxBlock extends Checkbox implements IAutoscrollRendering {
    readonly hideRequiredIndicatorFromName = true;

    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.placeholder && props.name}
                {props.description}
                <CheckboxFabric
                    styles={props.styles.checkboxes}
                    value={this.checkboxSlot}
                    required={this.required}
                    error={props.isFailed}
                    label={props.label}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
