import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Paragraph } from "tripetto-block-paragraph/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { BlockCaption } from "@ui/blocks/caption";
import { BlockImage } from "@ui/blocks/image";
import { BlockVideo } from "@ui/blocks/video";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-paragraph",
    alias: "paragraph",
})
export class ParagraphBlock extends Paragraph implements IAutoscrollRendering {
    readonly autoFocus = true;

    render(props: IAutoscrollRenderProps): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.name}
                {this.props.caption && <BlockCaption>{props.markdownifyToJSX(this.props.caption)}</BlockCaption>}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {this.props.video && (
                    <BlockVideo src={props.markdownifyToURL(this.props.video)} play={props.isActivated} view={props.view} />
                )}
            </>
        );
    }
}
