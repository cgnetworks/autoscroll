import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Matrix } from "tripetto-block-matrix/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { MatrixFabric } from "tripetto-runner-fabric/components/matrix";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-matrix",
})
export class MatrixBlock extends Matrix implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <MatrixFabric
                    styles={props.styles.matrix}
                    columns={this.columns(props)}
                    rows={this.rows(props)}
                    ariaDescribedBy={props.ariaDescribedBy}
                    allowUnselect={true}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
