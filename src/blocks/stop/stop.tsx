import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Stop } from "tripetto-block-stop/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-stop",
})
export class StopBlock extends Stop implements IAutoscrollRendering {
    readonly hideButtons = true;

    render(props: IAutoscrollRenderProps): ReactNode {
        return (
            (props.name || props.description || this.props.imageURL) && (
                <>
                    {this.props.imageURL && this.props.imageAboveText && (
                        <BlockImage
                            src={props.markdownifyToImage(this.props.imageURL)}
                            width={this.props.imageWidth}
                            isPage={props.isPage}
                        />
                    )}
                    {props.name}
                    {props.description}
                    {this.props.imageURL && !this.props.imageAboveText && (
                        <BlockImage
                            src={props.markdownifyToImage(this.props.imageURL)}
                            width={this.props.imageWidth}
                            isPage={props.isPage}
                        />
                    )}
                </>
            )
        );
    }
}
