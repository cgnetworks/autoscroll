import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Password } from "tripetto-block-password/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { PasswordFabric } from "tripetto-runner-fabric/components/password";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-password",
})
export class PasswordBlock extends Password implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <PasswordFabric
                    id={props.id}
                    styles={props.styles.inputs}
                    value={this.passwordSlot}
                    required={this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
