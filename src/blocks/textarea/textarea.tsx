import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Textarea } from "tripetto-block-textarea/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { TextareaFabric } from "tripetto-runner-fabric/components/textarea";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-textarea",
})
export class TextareaBlock extends Textarea implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <TextareaFabric
                    id={props.id}
                    styles={props.styles.inputs}
                    value={this.textareaSlot}
                    required={this.required}
                    error={props.isFailed}
                    autoSize={true}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    maxLength={this.maxLength}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
