import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Scale } from "tripetto-block-scale/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { ScaleFabric } from "tripetto-runner-fabric/components/scale";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-scale",
})
export class ScaleBlock extends Scale implements IAutoscrollRendering {
    readonly hideButtons = true;
    readonly autoSubmit = true;

    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <ScaleFabric
                    styles={props.styles.scale}
                    view={props.view}
                    options={this.options}
                    value={this.scaleSlot}
                    required={this.required}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    labelLeft={this.props.labelLeft && props.markdownifyToJSX(this.props.labelLeft, false)}
                    labelCenter={this.props.labelCenter && props.markdownifyToJSX(this.props.labelCenter, false)}
                    labelRight={this.props.labelRight && props.markdownifyToJSX(this.props.labelRight, false)}
                    justify={this.props.justify}
                    autoSubmit={this.autoSubmit}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
