import { ReactNode } from "react";
import { findFirst, tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { PictureChoice } from "tripetto-block-picture-choice/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { BlockCaption } from "@ui/blocks/caption";
import { PictureChoiceFabric } from "tripetto-runner-fabric/components/picture-choice";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-picture-choice",
    autoRender: true,
})
export class PictureChoiceBlock extends PictureChoice implements IAutoscrollRendering {
    readonly hideButtons = !this.props.multiple && findFirst(this.props.choices, (choice) => !!choice.name && !choice.url) ? true : false;
    readonly autoSubmit = this.hideButtons;

    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.name}
                {this.props.caption && <BlockCaption>{props.markdownifyToJSX(this.props.caption)}</BlockCaption>}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <PictureChoiceFabric
                    styles={props.styles.pictureChoice}
                    view={props.view}
                    options={this.choices(props)}
                    size={this.props.size}
                    value={(!this.props.multiple && this.valueOf("choice")) || undefined}
                    required={this.required}
                    ariaDescribedBy={props.ariaDescribedBy}
                    autoSubmit={this.autoSubmit}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
