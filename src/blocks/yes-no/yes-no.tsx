import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { YesNo } from "tripetto-block-yes-no/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { YesNoFabric } from "tripetto-runner-fabric/components/yes-no";
import { BlockImage } from "@ui/blocks/image";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-yes-no",
    alias: "yes-no",
})
export class YesNoBlock extends YesNo implements IAutoscrollRendering {
    readonly hideButtons = true;
    readonly autoSubmit = true;

    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.name}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <YesNoFabric
                    styles={props.styles.yesNo}
                    value={this.answerSlot}
                    yes={{
                        label: props.markdownifyToString(this.props.altYes || "") || props.l10n.pgettext("runner#6|🔷 Yes/No", "Yes"),
                    }}
                    no={{
                        label: props.markdownifyToString(this.props.altNo || "") || props.l10n.pgettext("runner#6|🔷 Yes/No", "No"),
                    }}
                    required={this.required}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    autoSubmit={this.autoSubmit}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
