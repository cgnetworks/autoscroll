import { ReactNode } from "react";
import { tripetto } from "tripetto-runner-foundation";
import { namespace } from "@namespace";
import { Radiobuttons } from "tripetto-block-radiobuttons/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { RadiobuttonsFabric } from "tripetto-runner-fabric/components/radiobuttons";

@tripetto({
    namespace,
    type: "node",
    identifier: "tripetto-block-radiobuttons",
})
export class RadiobuttonsBlock extends Radiobuttons implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <RadiobuttonsFabric
                    styles={props.styles.radiobuttons}
                    view={props.view}
                    buttons={this.buttons(props)}
                    value={this.radioSlot}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    allowUnselect={false}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
