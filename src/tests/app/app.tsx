import ReactDOM from "react-dom";
import Superagent from "superagent";
import { createRef } from "react";
import { Builder, IBuilderChangeEvent, IBuilderEditEvent, IBuilderReadyEvent, IDefinition, pgettext } from "tripetto";
import { Export, ISnapshot, TL10n } from "tripetto-runner-foundation";
import { IAutoscrollSnapshot, IAutoscrollStyles, run } from "../..";
import { Header } from "./header/header";
import STYLES_CONTRACT from "../../styles";
import L10N_CONTRACT from "../../../builder/l10n";
import "../../builder";

declare const PACKAGE_NAME: string;

const DEFINITION = PACKAGE_NAME + ":definition";
const SNAPSHOT = PACKAGE_NAME + ":snapshot";
const STYLES = PACKAGE_NAME + ":styles";
const L10N = PACKAGE_NAME + ":l10n";

// For this demo app we use the local store to save the definition, snapshot, styles and l10n settings.
// Here we try to retrieve that saved data.
const definition: IDefinition | undefined = JSON.parse(localStorage.getItem(DEFINITION) || "null") || undefined;
const snapshot: ISnapshot<IAutoscrollSnapshot> | undefined = JSON.parse(localStorage.getItem(SNAPSHOT) || "null") || undefined;
const styles: IAutoscrollStyles | undefined = JSON.parse(localStorage.getItem(STYLES) || "null") || undefined;
const l10n: TL10n | undefined = JSON.parse(localStorage.getItem(L10N) || "null") || undefined;

// Create a builder instance.
const builder = Builder.open(definition, {
    element: document.getElementById("builder"),
    fonts: "fonts/",
    disableSaveButton: true,
    disableRestoreButton: true,
    disableClearButton: true,
    disableCloseButton: true,
    disableOpenCloseAnimation: true,
    zoom: "fit-horizontal",
});

// Wait until the builder is ready!
builder.hook("OnReady", "synchronous", async (builderEvent: IBuilderReadyEvent) => {
    const updateRef = createRef<(() => void) | undefined>();
    const runner = await run({
        element: document.getElementById("runner"),
        definition: builderEvent.definition,
        view: "test",
        snapshot,
        styles,
        l10n,
        /*
        onImport: (i) => {
            Import.fields(i, [
                {
                    name: "NAME",
                    value: "Test",
                },
            ]);
        },
        */
        onChange: () => {
            if (updateRef.current) {
                updateRef.current();
            }
        },
        onAction: (t, f, b) => {
            console.log(t);
            console.log(f);
            console.log(b);
        },
        onSubmit: (instance, language, locale, namespace) =>
            new Promise((resolve: (reference: string) => void, reject: (reason?: string) => void) => {
                console.log(`Form submitted using runner ${namespace}!`);
                console.log(`Language: ${language} / Locale: ${locale}`);

                // Output the node props
                /*
                Export.exportables(instance).fields.forEach((field) => {
                    console.log(field.node.props());
                });
                */

                // Output the collected data to the console for demo purposes.
                console.dir(Export.exportables(instance));

                // Output can also be exported as CSV for your convenience.
                console.dir(Export.CSV(instance));

                setTimeout(() => {
                    resolve("DEMO123");
                }, 1000);

                /*
                setTimeout(() => reject("outdated"), 2000);
                setTimeout(() => reject("rejected"), 2000);
                setTimeout(() => reject("Simulating a connection error."), 2000);
                */
            }),
        onReload: () =>
            new Promise((resolve: (definition: IDefinition) => void, reject: () => void) => {
                if (definition) {
                    setTimeout(() => resolve(definition), 1000);
                } else {
                    reject();
                }
            }),
        onComplete: (instance, id) => {
            console.log(`OnComplete: ${id}`);
            console.dir(Export.exportables(instance));
            console.dir(Export.NVPs(instance));
            console.dir(Export.NVPs(instance, "strings"));
        },
        onPause: {
            recipe: "email",
            onPause: (emailAddress, snapshotData, language, locale, namespace) =>
                new Promise((resolve: () => void, reject: (reason?: string) => void) => {
                    console.log(`Pausing using ${namespace} to ${emailAddress}`);
                    console.log(`Language: ${language} / Locale: ${locale}`);
                    console.dir(snapshotData);

                    try {
                        localStorage.setItem(SNAPSHOT, JSON.stringify(snapshotData));
                    } catch {
                        console.log("Error while saving snapshot to localStorage.");
                    }

                    setTimeout(() => resolve(), 2000);
                    /*
                    setTimeout(() => reject("Simulating a connection error."), 2000);
                    */
                }),
        },
        /*
        onPause: (snapshotData) => {
            console.dir(snapshotData);
        },
        */
        onEdit: (type, id?) => {
            switch (type) {
                case "styles":
                    builder.stylesEditor(STYLES_CONTRACT(pgettext), runner.styles, "premium", (newStyles) => {
                        localStorage.setItem(STYLES, JSON.stringify((runner.styles = newStyles)));

                        console.dir(newStyles);
                    });
                    break;
                case "l10n":
                    builder.l10nEditor(L10N_CONTRACT(), runner.l10n, (newL10n) => {
                        localStorage.setItem(L10N, JSON.stringify((runner.l10n = newL10n)));

                        console.dir(newL10n);
                    });
                    break;
                case "prologue":
                    builder.edit("prologue");
                    break;
                case "epilogue":
                    builder.edit("epilogue", id);
                    break;
                default:
                    if (id) {
                        builder.edit("node", id);
                    }
                    break;
            }
        },
    });

    ReactDOM.render(
        <Header
            builder={builder}
            runner={runner}
            updateRef={updateRef}
            editStyles={() => {
                builder.stylesEditor(STYLES_CONTRACT(pgettext), runner.styles, "premium", (newStyles) => {
                    localStorage.setItem(STYLES, JSON.stringify((runner.styles = newStyles)));

                    console.dir(newStyles);
                });
            }}
            editL10n={() => {
                builder.l10nEditor(L10N_CONTRACT(), runner.l10n, (newL10n) => {
                    localStorage.setItem(L10N, JSON.stringify((runner.l10n = newL10n)));

                    console.dir(newL10n);
                });
            }}
            onLoadExample={() => {
                Superagent.get("example.json").end((error: {}, response: Superagent.Response) => {
                    if (response.ok) {
                        builder.definition = JSON.parse(response.text);
                    }
                });
            }}
            onClear={() => {
                builder.clear();
            }}
        />,
        document.getElementById("header")
    );

    // Store the definition in the local store upon each builder change and reload the runner
    builder.hook("OnChange", "synchronous", (changeEvent: IBuilderChangeEvent) => {
        runner.definition = changeEvent.definition;

        try {
            localStorage.setItem(DEFINITION, JSON.stringify(changeEvent.definition));
            localStorage.removeItem(SNAPSHOT);
        } catch {
            console.log("Error while saving definition to localStorage.");
        }
    });

    // When a prologue, block or epilogue is edited, we can preview that in the runner.
    builder.hook("OnEdit", "synchronous", (editEvent: IBuilderEditEvent) => runner.doPreview(editEvent.data));

    // When the host window resizes, we should notify the builder.
    window.addEventListener("resize", () => builder.resize());
    window.addEventListener("orientationchange", () => builder.resize());
});
