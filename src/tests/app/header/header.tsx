import styled from "styled-components";
import { MutableRefObject, RefObject, useState } from "react";
import { Builder } from "tripetto";
import { TRunnerViews } from "tripetto-runner-react-hook";
import { IAutoscrollRunner } from "../../..";

declare const PACKAGE_NAME: string;
declare const PACKAGE_TITLE: string;
declare const PACKAGE_VERSION: string;

const LeftSection = styled.div`
    display: flex;
    flex-grow: 1;
    padding: 16px;
    white-space: nowrap;
    overflow: hidden;
`;

const RightSection = styled.div`
    display: flex;
    padding: 16px;
    padding-left: 0;
    white-space: nowrap;
`;

const BuilderButton = styled.button<{
    isBuilderVisible: boolean;
}>`
    display: inline-block;
    background: #000
        url("data:image/svg+xml;base64,${(props) =>
            props.isBuilderVisible
                ? "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTExLjUgMTRoLThjLTAuODI3IDAtMS41LTAuNjczLTEuNS0xLjV2LThjMC0wLjgyNyAwLjY3My0xLjUgMS41LTEuNWgxM2MwLjgyNyAwIDEuNSAwLjY3MyAxLjUgMS41djFjMCAwLjI3Ni0wLjIyNCAwLjUtMC41IDAuNXMtMC41LTAuMjI0LTAuNS0wLjV2LTFjMC0wLjI3Ni0wLjIyNC0wLjUtMC41LTAuNWgtMTNjLTAuMjc2IDAtMC41IDAuMjI0LTAuNSAwLjV2OGMwIDAuMjc2IDAuMjI0IDAuNSAwLjUgMC41aDhjMC4yNzYgMCAwLjUgMC4yMjQgMC41IDAuNXMtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSIjZmZmIj48L3BhdGg+CjxwYXRoIGQ9Ik0xMS41IDE4aC0xMGMtMC44MjcgMC0xLjUtMC42NzMtMS41LTEuNXYtMWMwLTAuMjc2IDAuMjI0LTAuNSAwLjUtMC41aDExYzAuMjc2IDAgMC41IDAuMjI0IDAuNSAwLjVzLTAuMjI0IDAuNS0wLjUgMC41aC0xMC41djAuNWMwIDAuMjc2IDAuMjI0IDAuNSAwLjUgMC41aDEwYzAuMjc2IDAgMC41IDAuMjI0IDAuNSAwLjVzLTAuMjI0IDAuNS0wLjUgMC41eiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8cGF0aCBkPSJNMTguNSAxOGgtNGMtMC44MjcgMC0xLjUtMC42NzMtMS41LTEuNXYtOGMwLTAuODI3IDAuNjczLTEuNSAxLjUtMS41aDRjMC44MjcgMCAxLjUgMC42NzMgMS41IDEuNXY4YzAgMC44MjctMC42NzMgMS41LTEuNSAxLjV6TTE0LjUgOGMtMC4yNzYgMC0wLjUgMC4yMjQtMC41IDAuNXY4YzAgMC4yNzYgMC4yMjQgMC41IDAuNSAwLjVoNGMwLjI3NiAwIDAuNS0wLjIyNCAwLjUtMC41di04YzAtMC4yNzYtMC4yMjQtMC41LTAuNS0wLjVoLTR6IiBmaWxsPSIjZmZmIj48L3BhdGg+CjxwYXRoIGQ9Ik0xNi41IDE2Yy0wLjEzMiAwLTAuMjYtMC4wNTMtMC4zNTMtMC4xNDdzLTAuMTQ3LTAuMjIyLTAuMTQ3LTAuMzUzIDAuMDUzLTAuMjYxIDAuMTQ3LTAuMzUzYzAuMDkzLTAuMDkzIDAuMjIyLTAuMTQ3IDAuMzUzLTAuMTQ3czAuMjYxIDAuMDUzIDAuMzUzIDAuMTQ3YzAuMDkzIDAuMDkzIDAuMTQ3IDAuMjIyIDAuMTQ3IDAuMzUzcy0wLjA1MyAwLjI2MS0wLjE0NyAwLjM1M2MtMC4wOTMgMC4wOTMtMC4yMjIgMC4xNDctMC4zNTMgMC4xNDd6IiBmaWxsPSIjZmZmIj48L3BhdGg+Cjwvc3ZnPgo="
                : "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTE5LjEwNCAwLjg5NmMtMC41NjItMC41NjItMS4zMDktMC44NzEtMi4xMDQtMC44NzFzLTEuNTQyIDAuMzA5LTIuMTA0IDAuODcxbC0xMi43NSAxMi43NWMtMC4wNTIgMC4wNTItMC4wOTEgMC4xMTQtMC4xMTYgMC4xODNsLTIgNS41Yy0wLjA2NiAwLjE4My0wLjAyMSAwLjM4NyAwLjExNiAwLjUyNCAwLjA5NSAwLjA5NSAwLjIyMyAwLjE0NiAwLjM1NCAwLjE0NiAwLjA1NyAwIDAuMTE1LTAuMDEwIDAuMTcxLTAuMDMwbDUuNS0yYzAuMDY5LTAuMDI1IDAuMTMxLTAuMDY1IDAuMTgzLTAuMTE2bDEyLjc1LTEyLjc1YzAuNTYyLTAuNTYyIDAuODcxLTEuMzA5IDAuODcxLTIuMTA0cy0wLjMwOS0xLjU0Mi0wLjg3MS0yLjEwNHpNNS43MjUgMTcuMDY4bC00LjM4OSAxLjU5NiAxLjU5Ni00LjM4OSAxMS4wNjgtMTEuMDY4IDIuNzkzIDIuNzkzLTExLjA2OCAxMS4wNjh6TTE4LjM5NiA0LjM5NmwtMC44OTYgMC44OTYtMi43OTMtMi43OTMgMC44OTYtMC44OTZjMC4zNzMtMC4zNzMgMC44NjktMC41NzggMS4zOTYtMC41NzhzMS4wMjMgMC4yMDUgMS4zOTYgMC41NzhjMC4zNzMgMC4zNzMgMC41NzggMC44NjkgMC41NzggMS4zOTZzLTAuMjA1IDEuMDIzLTAuNTc4IDEuMzk2eiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8L3N2Zz4K"}")
        no-repeat center center/20px 20px;
    border: 0;
    font-family: sans-serif;
    min-width: 32px;
    height: 32px;
    margin-right: 8px;
    border-radius: 4px;
    outline: none;
    cursor: pointer;
    transition: box-shadow 0.2s ease-out, opacity 0.2s ease-out;
    appearance: none;

    :focus {
        outline: none;
        box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.15);
    }

    :hover:not(:disabled) {
        box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.15);
    }

    @media (min-width: 992px) {
        display: none;
    }
`;

const Title = styled.div<{
    unnamed: boolean;
}>`
    display: inline-block;
    font-family: sans-serif;
    font-size: 15px;
    font-weight: bold;
    color: #000;
    opacity: ${(props) => (props.unnamed ? 0.2 : 1)};
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    width: 100%;
    height: 32px;
    padding-left: 8px;
    padding-top: 8px;
`;

const Links = styled.div`
    display: inline-block;
    font-family: sans-serif;
    font-size: 11px;
    color: #888;
    white-space: nowrap;
    height: 32px;
    display: none;
    padding-top: 10px;

    > a {
        margin-top: 8px;
        margin-left: 16px;
        font-size: 11px;
        color: blue;
        text-decoration: none;

        :hover {
            text-decoration: underline;
            color: blue;
        }
    }

    @media (min-width: 840px) {
        display: block;
    }
`;

const ControlButton = styled.button<{
    icon: string;
    size: number;
}>`
    display: inline-block;
    background: #000 url("data:image/svg+xml;base64,${(props) => props.icon}") no-repeat center center/ ${(props) => props.size}px
        ${(props) => props.size}px;
    border: 0;
    font-family: sans-serif;
    min-width: 32px;
    height: 32px;
    margin-left: 8px;
    border-radius: 4px;
    outline: none;
    cursor: pointer;
    transition: box-shadow 0.2s ease-out, opacity 0.2s ease-out;
    appearance: none;

    :focus {
        outline: none;
        box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.15);
    }

    :hover:not(:disabled) {
        box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.15);
    }

    :disabled {
        opacity: 0.1;
        cursor: default;
        pointer-events: none;
    }
`;

const View = styled.select`
    display: none;
    height: 32px;
    border: none;
    background-color: #000;
    appearance: none;
    color: #fff;
    border-radius: 4px;
    margin: 0 0 0 8px;
    font-family: sans-serif;
    font-size: 14px;
    padding: 0 32px;
    transition: box-shadow 0.2s ease-out;
    background: url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTAgNmMwLTAuMTI4IDAuMDQ5LTAuMjU2IDAuMTQ2LTAuMzU0IDAuMTk1LTAuMTk1IDAuNTEyLTAuMTk1IDAuNzA3IDBsOC42NDYgOC42NDYgOC42NDYtOC42NDZjMC4xOTUtMC4xOTUgMC41MTItMC4xOTUgMC43MDcgMHMwLjE5NSAwLjUxMiAwIDAuNzA3bC05IDljLTAuMTk1IDAuMTk1LTAuNTEyIDAuMTk1LTAuNzA3IDBsLTktOWMtMC4wOTgtMC4wOTgtMC4xNDYtMC4yMjYtMC4xNDYtMC4zNTR6IiBmaWxsPSIjZmZmIj48L3BhdGg+Cjwvc3ZnPgo=")
            no-repeat right 8px center/16px 16px,
        url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTE5Ljg3MiAxMC4xNjZjLTAuMDQ3LTAuMDUzLTEuMTgyLTEuMzA1LTIuOTU2LTIuNTcyLTEuMDQ3LTAuNzQ4LTIuMS0xLjM0NC0zLjEzLTEuNzczLTEuMzA1LTAuNTQ0LTIuNTc5LTAuODItMy43ODYtMC44MnMtMi40ODEgMC4yNzYtMy43ODYgMC44MmMtMS4wMzAgMC40MjktMi4wODMgMS4wMjYtMy4xMyAxLjc3My0xLjc3NCAxLjI2Ny0yLjkwOSAyLjUyLTIuOTU2IDIuNTcyLTAuMTcxIDAuMTktMC4xNzEgMC40NzkgMCAwLjY2OSAwLjA0NyAwLjA1MyAxLjE4MiAxLjMwNSAyLjk1NiAyLjU3MiAxLjA0NyAwLjc0OCAyLjEgMS4zNDQgMy4xMyAxLjc3MyAxLjMwNSAwLjU0NCAyLjU3OSAwLjgyIDMuNzg2IDAuODJzMi40ODEtMC4yNzYgMy43ODYtMC44MmMxLjAzMC0wLjQyOSAyLjA4My0xLjAyNiAzLjEzLTEuNzczIDEuNzc0LTEuMjY3IDIuOTA5LTIuNTIgMi45NTYtMi41NzIgMC4xNzEtMC4xOSAwLjE3MS0wLjQ3OSAwLTAuNjY5ek0xMi41NzQgNi40MzhjMC45MDcgMC43NjMgMS40MjYgMS44NzMgMS40MjYgMy4wNjIgMCAyLjIwNi0xLjc5NCA0LTQgNHMtNC0xLjc5NC00LTRjMC0xLjE4OCAwLjUxOS0yLjI5OSAxLjQyNi0zLjA2MiAwLjgyMi0wLjI2OCAxLjY5MS0wLjQzOCAyLjU3NC0wLjQzOHMxLjc1MiAwLjE3IDIuNTc0IDAuNDM4ek0xNi4zMTcgMTIuNjA2Yy0xLjUzMyAxLjA5Mi0zLjg3MyAyLjM5NC02LjMxNyAyLjM5NHMtNC43ODQtMS4zMDItNi4zMTctMi4zOTRjLTEuMTU3LTAuODI0LTIuMDQyLTEuNjU4LTIuNDg5LTIuMTA2IDAuNDQ3LTAuNDQ4IDEuMzMyLTEuMjgxIDIuNDg5LTIuMTA2IDAuNTMtMC4zNzggMS4xNTYtMC43OCAxLjg1LTEuMTQ1LTAuMzQ3IDAuNjg4LTAuNTMzIDEuNDU1LTAuNTMzIDIuMjUxIDAgMi43NTcgMi4yNDMgNSA1IDVzNS0yLjI0MyA1LTVjMC0wLjc5Ni0wLjE4Ni0xLjU2My0wLjUzMy0yLjI1MSAwLjY5NCAwLjM2NSAxLjMyIDAuNzY4IDEuODUgMS4xNDUgMS4xNTcgMC44MjQgMi4wNDIgMS42NTggMi40ODkgMi4xMDYtMC40NDcgMC40NDgtMS4zMzIgMS4yODEtMi40ODkgMi4xMDZ6IiBmaWxsPSIjZmZmIj48L3BhdGg+Cjwvc3ZnPgo=")
            no-repeat left 8px center/16px 16px;
    background-color: #000;

    :focus {
        outline: none;
        box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.15);
    }

    :hover {
        box-shadow: 0 0 0 3px rgba(0, 0, 0, 0.15);
    }

    @media (min-width: 640px) {
        display: block;
    }

    ::-ms-expand {
        display: none;
    }
`;

export const Header = (props: {
    readonly builder: Builder;
    readonly runner: IAutoscrollRunner;
    readonly updateRef: RefObject<(() => void) | undefined>;
    readonly editStyles: () => void;
    readonly editL10n: () => void;
    readonly onLoadExample: () => void;
    readonly onClear: () => void;
}) => {
    const [builderVisible, setBuilderVisibility] = useState(false);
    const [, updateState] = useState({});
    const update = () => updateState({});

    (props.updateRef as MutableRefObject<(() => void) | undefined>).current = update;

    return (
        <>
            <LeftSection>
                <BuilderButton
                    onClick={() => {
                        setBuilderVisibility(!builderVisible);
                        document.getElementById("builder")!.classList.toggle("visible", !builderVisible);
                    }}
                    isBuilderVisible={builderVisible}
                />
                <Title unnamed={!props.runner.definition.name} onClick={() => props.builder.edit()}>
                    {props.runner.definition.name || "Unnamed"}
                </Title>
                <Links>
                    {PACKAGE_TITLE} v{PACKAGE_VERSION}
                    <a href={`https://www.npmjs.com/package/${PACKAGE_NAME}/`} target="_blank">
                        View readme
                    </a>
                    <a href="https://gitlab.com/tripetto/runners/autoscroll" target="_blank">
                        Get source
                    </a>
                    <a href="#" onClick={() => props.onLoadExample()}>
                        Load example
                    </a>
                    <a href="#" onClick={() => props.onClear()}>
                        Clear
                    </a>
                </Links>
            </LeftSection>
            <RightSection>
                <ControlButton
                    disabled={props.runner.view !== "live" || !props.runner.allowStart}
                    onClick={() => props.runner.start()}
                    size={24}
                    icon="PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTYuNSAxNmMtMC4wODMgMC0wLjE2Ny0wLjAyMS0wLjI0Mi0wLjA2My0wLjE1OS0wLjA4OC0wLjI1OC0wLjI1Ni0wLjI1OC0wLjQzN3YtMTBjMC0wLjE4MiAwLjA5OS0wLjM0OSAwLjI1OC0wLjQzN3MwLjM1My0wLjA4MyAwLjUwNyAwLjAxM2w4IDVjMC4xNDYgMC4wOTEgMC4yMzUgMC4yNTIgMC4yMzUgMC40MjRzLTAuMDg5IDAuMzMzLTAuMjM1IDAuNDI0bC04IDVjLTAuMDgxIDAuMDUxLTAuMTczIDAuMDc2LTAuMjY1IDAuMDc2ek03IDYuNDAydjguMTk2bDYuNTU3LTQuMDk4LTYuNTU3LTQuMDk4eiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8L3N2Zz4K"
                />
                <ControlButton
                    disabled={props.runner.view !== "live" || !props.runner.allowPause}
                    onClick={() => props.runner.pause()}
                    size={28}
                    icon="PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTcuNSAxNWgtMWMtMC44MjcgMC0xLjUtMC42NzMtMS41LTEuNXYtNmMwLTAuODI3IDAuNjczLTEuNSAxLjUtMS41aDFjMC44MjcgMCAxLjUgMC42NzMgMS41IDEuNXY2YzAgMC44MjctMC42NzMgMS41LTEuNSAxLjV6TTYuNSA3Yy0wLjI3NiAwLTAuNSAwLjIyNC0wLjUgMC41djZjMCAwLjI3NiAwLjIyNCAwLjUgMC41IDAuNWgxYzAuMjc2IDAgMC41LTAuMjI0IDAuNS0wLjV2LTZjMC0wLjI3Ni0wLjIyNC0wLjUtMC41LTAuNWgtMXoiIGZpbGw9IiNmZmYiPjwvcGF0aD4KPHBhdGggZD0iTTEyLjUgMTVoLTFjLTAuODI3IDAtMS41LTAuNjczLTEuNS0xLjV2LTZjMC0wLjgyNyAwLjY3My0xLjUgMS41LTEuNWgxYzAuODI3IDAgMS41IDAuNjczIDEuNSAxLjV2NmMwIDAuODI3LTAuNjczIDEuNS0xLjUgMS41ek0xMS41IDdjLTAuMjc2IDAtMC41IDAuMjI0LTAuNSAwLjV2NmMwIDAuMjc2IDAuMjI0IDAuNSAwLjUgMC41aDFjMC4yNzYgMCAwLjUtMC4yMjQgMC41LTAuNXYtNmMwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41aC0xeiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8L3N2Zz4K"
                />
                <ControlButton
                    disabled={props.runner.view !== "live" || !props.runner.allowStop}
                    onClick={() => props.runner.stop()}
                    size={28}
                    icon="PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTEyLjUgMTVoLTZjLTAuODI3IDAtMS41LTAuNjczLTEuNS0xLjV2LTZjMC0wLjgyNyAwLjY3My0xLjUgMS41LTEuNWg2YzAuODI3IDAgMS41IDAuNjczIDEuNSAxLjV2NmMwIDAuODI3LTAuNjczIDEuNS0xLjUgMS41ek02LjUgN2MtMC4yNzYgMC0wLjUgMC4yMjQtMC41IDAuNXY2YzAgMC4yNzYgMC4yMjQgMC41IDAuNSAwLjVoNmMwLjI3NiAwIDAuNS0wLjIyNCAwLjUtMC41di02YzAtMC4yNzYtMC4yMjQtMC41LTAuNS0wLjVoLTZ6IiBmaWxsPSIjZmZmIj48L3BhdGg+Cjwvc3ZnPgo="
                />
                <ControlButton
                    disabled={!props.runner.allowRestart}
                    onClick={() => props.runner.restart()}
                    size={20}
                    icon="PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTE3LjUxIDQuNDljLTEuNjA1LTEuNjA1LTMuNzQtMi40OS02LjAxMC0yLjQ5cy00LjQwNSAwLjg4NC02LjAxMCAyLjQ5LTIuNDkgMy43NC0yLjQ5IDYuMDEwdjEuMjkzbC0yLjE0Ni0yLjE0NmMtMC4xOTUtMC4xOTUtMC41MTItMC4xOTUtMC43MDcgMHMtMC4xOTUgMC41MTIgMCAwLjcwN2wzIDNjMC4wOTggMC4wOTggMC4yMjYgMC4xNDYgMC4zNTQgMC4xNDZzMC4yNTYtMC4wNDkgMC4zNTQtMC4xNDZsMy0zYzAuMTk1LTAuMTk1IDAuMTk1LTAuNTEyIDAtMC43MDdzLTAuNTEyLTAuMTk1LTAuNzA3IDBsLTIuMTQ2IDIuMTQ2di0xLjI5M2MwLTQuMTM2IDMuMzY0LTcuNSA3LjUtNy41czcuNSAzLjM2NCA3LjUgNy41LTMuMzY0IDcuNS03LjUgNy41Yy0wLjI3NiAwLTAuNSAwLjIyNC0wLjUgMC41czAuMjI0IDAuNSAwLjUgMC41YzIuMjcgMCA0LjQwNS0wLjg4NCA2LjAxMC0yLjQ5czIuNDktMy43NCAyLjQ5LTYuMDEwYzAtMi4yNy0wLjg4NC00LjQwNS0yLjQ5LTYuMDEweiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8L3N2Zz4K"
                />
                <View
                    defaultValue={props.runner.view}
                    onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                        props.runner.view = e.target.value as TRunnerViews;
                    }}
                    title="Switch view mode"
                >
                    <option value="preview">Preview</option>
                    <option value="test">Test</option>
                    {document.domain === "localhost" && <option value="live">Live</option>}
                </View>
                <ControlButton
                    onClick={() => props.editStyles()}
                    size={20}
                    icon="PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTYuNSA4Yy0wLjgyNyAwLTEuNS0wLjY3My0xLjUtMS41czAuNjczLTEuNSAxLjUtMS41IDEuNSAwLjY3MyAxLjUgMS41LTAuNjczIDEuNS0xLjUgMS41ek02LjUgNmMtMC4yNzYgMC0wLjUgMC4yMjQtMC41IDAuNXMwLjIyNCAwLjUgMC41IDAuNSAwLjUtMC4yMjQgMC41LTAuNS0wLjIyNC0wLjUtMC41LTAuNXoiIGZpbGw9IiNmZmYiPjwvcGF0aD4KPHBhdGggZD0iTTMuNSAxMWMtMC44MjcgMC0xLjUtMC42NzMtMS41LTEuNXMwLjY3My0xLjUgMS41LTEuNSAxLjUgMC42NzMgMS41IDEuNWMwIDAuODI3LTAuNjczIDEuNS0xLjUgMS41ek0zLjUgOWMtMC4yNzYgMC0wLjUgMC4yMjQtMC41IDAuNXMwLjIyNCAwLjUgMC41IDAuNSAwLjUtMC4yMjQgMC41LTAuNS0wLjIyNC0wLjUtMC41LTAuNXoiIGZpbGw9IiNmZmYiPjwvcGF0aD4KPHBhdGggZD0iTTEwLjUgN2MtMC44MjcgMC0xLjUtMC42NzMtMS41LTEuNXMwLjY3My0xLjUgMS41LTEuNWMwLjgyNyAwIDEuNSAwLjY3MyAxLjUgMS41cy0wLjY3MyAxLjUtMS41IDEuNXpNMTAuNSA1Yy0wLjI3NiAwLTAuNSAwLjIyNC0wLjUgMC41czAuMjI0IDAuNSAwLjUgMC41IDAuNS0wLjIyNCAwLjUtMC41LTAuMjI0LTAuNS0wLjUtMC41eiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8cGF0aCBkPSJNMTAgMTljLTIuNjU1IDAtNS4xNTUtMC44NzQtNy4wNDAtMi40NjEtMC45MjUtMC43NzktMS42NTItMS42ODctMi4xNjEtMi43MDEtMC41MzEtMS4wNTctMC44LTIuMTgtMC44LTMuMzM4czAuMjY5LTIuMjgyIDAuOC0zLjMzOGMwLjUwOS0xLjAxNCAxLjIzNi0xLjkyMiAyLjE2MS0yLjcwMSAxLjg4NC0xLjU4NyA0LjM4NC0yLjQ2MSA3LjA0MC0yLjQ2MXM1LjE1NSAwLjg3NCA3LjA0MCAyLjQ2MWMwLjkyNSAwLjc3OSAxLjY1MiAxLjY4NyAyLjE2MSAyLjcwMSAwLjUzMSAxLjA1NyAwLjggMi4xOCAwLjggMy4zMzggMCAxLjg1Mi0wLjgyNSAyLjUxMi0xLjU5NyAyLjUxMi0wLjY0NSAwLTEuMzA5LTAuNDUyLTEuODIyLTEuMjM5LTAuMzI0LTAuNDk3LTAuNjY2LTAuNzcxLTAuOTYzLTAuNzcxLTAuMDk5IDAtMC4xOTcgMC4wMzAtMC4yOTkgMC4wOTEtMC41NjIgMC4zMzYtMC4yMyAxLjI0NSAwLjAxOCAxLjc1NiAwLjgwMiAxLjY1NCAwLjg2OCAzLjA5OCAwLjE5MiA0LjE3OC0wLjgyIDEuMzEtMi42ODEgMS45NzQtNS41MyAxLjk3NHpNMTAgM2MtNC45NjMgMC05IDMuMzY0LTkgNy41czQuMDM3IDcuNSA5IDcuNWMxLjM2NiAwIDIuNDc0LTAuMTU5IDMuMjkzLTAuNDczIDAuNjQ5LTAuMjQ5IDEuMTE3LTAuNTk2IDEuMzktMS4wMzIgMC42MTEtMC45NzYgMC4yMDQtMi4yODQtMC4yNDUtMy4yMS0wLjk2LTEuOTgtMC4wNjMtMi43OTMgMC4zNjgtMy4wNTEgMC4yNTgtMC4xNTQgMC41MzEtMC4yMzMgMC44MTMtMC4yMzMgMC40NTggMCAxLjE0MiAwLjIxMyAxLjgwMSAxLjIyNiAwLjM3NyAwLjU3OSAwLjc2NiAwLjc4NSAwLjk4NCAwLjc4NSAwLjM1NyAwIDAuNTk3LTAuNjA4IDAuNTk3LTEuNTEyIDAtNC4xMzYtNC4wMzctNy41LTktNy41eiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8cGF0aCBkPSJNMTEgMTZjLTEuMTAzIDAtMi0wLjg5Ny0yLTJzMC44OTctMiAyLTJjMS4xMDMgMCAyIDAuODk3IDIgMnMtMC44OTcgMi0yIDJ6TTExIDEzYy0wLjU1MSAwLTEgMC40NDktMSAxczAuNDQ5IDEgMSAxIDEtMC40NDkgMS0xLTAuNDQ5LTEtMS0xeiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8L3N2Zz4K"
                    title="Edit styles"
                />
                <ControlButton
                    onClick={() => props.editL10n()}
                    size={20}
                    icon="PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0ZWQgYnkgSWNvTW9vbi5pbyAtLT4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTQuNSAxNGMtMC4yNzYgMC0wLjUgMC4yMjQtMC41IDAuNXYwLjAwMWMtMC40MTgtMC4zMTUtMC45MzgtMC41MDEtMS41LTAuNTAxLTEuMzc4IDAtMi41IDEuMTIyLTIuNSAyLjVzMS4xMjIgMi41IDIuNSAyLjVjMC41NjIgMCAxLjA4Mi0wLjE4NyAxLjUtMC41MDF2MC4wMDFjMCAwLjI3NiAwLjIyNCAwLjUgMC41IDAuNXMwLjUtMC4yMjQgMC41LTAuNXYtNGMwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41ek0yLjUgMThjLTAuODI3IDAtMS41LTAuNjczLTEuNS0xLjVzMC42NzMtMS41IDEuNS0xLjUgMS41IDAuNjczIDEuNSAxLjUtMC42NzMgMS41LTEuNSAxLjV6IiBmaWxsPSIjZmZmIj48L3BhdGg+CjxwYXRoIGQ9Ik04LjUgMTRjLTAuNTYyIDAtMS4wODIgMC4xODctMS41IDAuNTAxdi00LjAwMWMwLTAuMjc2LTAuMjI0LTAuNS0wLjUtMC41cy0wLjUgMC4yMjQtMC41IDAuNXY4YzAgMC4yNzYgMC4yMjQgMC41IDAuNSAwLjVzMC41LTAuMjI0IDAuNS0wLjV2LTAuMDAxYzAuNDE4IDAuMzE1IDAuOTM4IDAuNTAxIDEuNSAwLjUwMSAxLjM3OCAwIDIuNS0xLjEyMiAyLjUtMi41cy0xLjEyMi0yLjUtMi41LTIuNXpNOC41IDE4Yy0wLjgyNyAwLTEuNS0wLjY3My0xLjUtMS41czAuNjczLTEuNSAxLjUtMS41IDEuNSAwLjY3MyAxLjUgMS41LTAuNjczIDEuNS0xLjUgMS41eiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8cGF0aCBkPSJNMTQuNSAxOWMtMS4zNzggMC0yLjUtMS4xMjItMi41LTIuNXMxLjEyMi0yLjUgMi41LTIuNWMwLjQzOSAwIDAuODcyIDAuMTE2IDEuMjUgMC4zMzUgMC4yMzkgMC4xMzggMC4zMjEgMC40NDQgMC4xODMgMC42ODNzLTAuNDQ0IDAuMzIxLTAuNjgzIDAuMTgzYy0wLjIyNy0wLjEzMS0wLjQ4Ni0wLjItMC43NS0wLjItMC44MjcgMC0xLjUgMC42NzMtMS41IDEuNXMwLjY3MyAxLjUgMS41IDEuNWMwLjI2NCAwIDAuNTIzLTAuMDY5IDAuNzUtMC4yIDAuMjM5LTAuMTM4IDAuNTQ1LTAuMDU3IDAuNjgzIDAuMTgyczAuMDU3IDAuNTQ1LTAuMTgyIDAuNjgzYy0wLjM3OSAwLjIxOS0wLjgxMSAwLjMzNS0xLjI1IDAuMzM1eiIgZmlsbD0iI2ZmZiI+PC9wYXRoPgo8cGF0aCBkPSJNMTEuNSAxMC41Yy0wLjEyOCAwLTAuMjU2LTAuMDQ5LTAuMzU0LTAuMTQ2bC0zLTNjLTAuMTk1LTAuMTk1LTAuMTk1LTAuNTEyIDAtMC43MDdzMC41MTItMC4xOTUgMC43MDcgMGwyLjY0NiAyLjY0NiA2LjY0Ni02LjY0NmMwLjE5NS0wLjE5NSAwLjUxMi0wLjE5NSAwLjcwNyAwczAuMTk1IDAuNTEyIDAgMC43MDdsLTcgN2MtMC4wOTggMC4wOTgtMC4yMjYgMC4xNDYtMC4zNTQgMC4xNDZ6IiBmaWxsPSIjZmZmIj48L3BhdGg+Cjwvc3ZnPgo="
                    title="Edit text labels"
                />
            </RightSection>
        </>
    );
};
