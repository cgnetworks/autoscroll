export interface IElement {
    readonly index: number;
    readonly key: string;
    readonly ref: HTMLElement;
    width: number;
    height: number;
    readonly top: number;
    readonly left: number;
    readonly isFirst: boolean;
    readonly isLast: boolean;
}

export type TElement = (key: string) => (element: HTMLElement | null) => void;
