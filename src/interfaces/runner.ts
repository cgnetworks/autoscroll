import { IAutoscrollController } from "@hooks/controller";

export interface IAutoscrollRunner extends IAutoscrollController {
    readonly destroy: () => void;
}
