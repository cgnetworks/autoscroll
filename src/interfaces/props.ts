import { IRunnerProps, TRunnerViews } from "tripetto-runner-react-hook";
import { IDefinition, ISnapshot, TL10n } from "tripetto-runner-foundation";
import { CSSProperties } from "react";
import { IAutoscrollController } from "@hooks/controller";
import { IAutoscrollSnapshot } from "./snapshot";
import { IAutoscrollStyles } from "./styles";

export type TAutoscrollDisplay = "inline" | "page";

export type TAutoscrollPause =
    | {
          readonly recipe: "email";
          readonly onPause: (
              emailAddress: string,
              snapshot: ISnapshot,
              language: string,
              locale: string,
              namespace: string
          ) => Promise<void> | boolean | void;
      }
    | ((snapshot: ISnapshot, language: string, locale: string, namespace: string) => Promise<void> | boolean | void);

export interface IAutoscrollProps extends IRunnerProps<IAutoscrollSnapshot> {
    readonly styles?: IAutoscrollStyles;
    readonly l10n?: TL10n;
    readonly view?: TRunnerViews;
    readonly display?: TAutoscrollDisplay;
    readonly controller?: [IAutoscrollController | undefined];
    readonly className?: string;
    readonly customStyle?: CSSProperties;
    readonly customCSS?: string;
    readonly onL10n?: (l10n: TL10n) => Promise<void>;
    readonly onReload?: () => IDefinition | Promise<IDefinition>;
    readonly onEdit?: (type: "prologue" | "epilogue" | "styles" | "l10n" | "block", id?: string) => void;
    readonly onPause?: TAutoscrollPause;
    readonly onTouch?: () => void;
}
