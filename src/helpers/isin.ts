import { MutableRefObject } from "react";
import { IViewport } from "@interfaces/viewport";
import { Num } from "tripetto-runner-foundation";

export function isIn(element: HTMLElement, viewportRef: MutableRefObject<IViewport>): boolean {
    const rect = element.getBoundingClientRect();

    return (
        Num.inRange(rect.left, viewportRef.current.left, viewportRef.current.left + viewportRef.current.width) &&
        Num.inRange(rect.right, viewportRef.current.left, viewportRef.current.left + viewportRef.current.width) &&
        Num.inRange(rect.top, viewportRef.current.top, viewportRef.current.top + viewportRef.current.height) &&
        Num.inRange(rect.bottom, viewportRef.current.top, viewportRef.current.top + viewportRef.current.height)
    );
}
