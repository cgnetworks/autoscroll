import { each, findFirst } from "tripetto-runner-foundation";

interface IRef {
    readonly update: (force: boolean) => void;
    readonly index: number;
}

const refs: {
    [index: number]: IRef | undefined;
} = {};

const sources: {
    [id: string]: boolean | undefined;
} = {};

let index = 0;

export const attach = (update: (force: boolean) => void): IRef => {
    while (refs[index]) {
        index++;
    }

    update(false);

    return (refs[index] = {
        update,
        index,
    });
};

export const detach = (ref: IRef): void => {
    index = ref.index;

    delete refs[ref.index];
};

export const useLoader = (id: string) => {
    if (typeof sources[id] !== "boolean") {
        sources[id] = false;
    }

    return () => {
        if (!sources[id]) {
            sources[id] = true;

            if (!isLoading()) {
                each(refs, (ref: IRef | undefined) => {
                    if (ref) {
                        ref.update(true);
                    }
                });
            }
        }
    };
};

export const isLoading = () => typeof findFirst(sources, (source) => !source) === "boolean";
