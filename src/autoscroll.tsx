import { StrictMode } from "react";
import { useAutoscrollRunner } from "@hooks/runner";
import { IAutoscrollProps } from "@interfaces/props";
import { AutoscrollRoot } from "@ui/root";
import { EmptyMessage } from "@ui/messages/empty";
import { ClosedMessage } from "@ui/messages/closed";
import { PausedMessage } from "@ui/messages/paused";
import { Prologue } from "@ui/messages/prologue";
import { Epilogue } from "@ui/messages/epilogue";
import "@blocks";

export const AutoscrollRunner = (props: IAutoscrollProps) => {
    const {
        status,
        frameRef,
        view,
        isPage,
        title,
        height,
        disableScrolling,
        prologue,
        blocks,
        epilogue,
        l10n,
        styles,
        navigation,
        onResize,
        onScroll,
        onKey,
    } = useAutoscrollRunner(props);

    return (
        <StrictMode>
            <AutoscrollRoot
                frameRef={frameRef}
                styles={styles}
                view={view}
                isPage={isPage}
                title={title}
                height={height}
                disableScrolling={disableScrolling}
                navigation={navigation}
                onResize={onResize}
                onScroll={onScroll}
                onKey={onKey}
                onTouch={props.onTouch}
                className={props.className}
                customStyle={props.customStyle}
                customCSS={props.customCSS}
            >
                {(prologue && <Prologue {...prologue} />) ||
                    blocks ||
                    (epilogue && <Epilogue {...epilogue} />) ||
                    (view !== "live" && (status === "empty" || status === "preview") && (
                        <EmptyMessage l10n={l10n} styles={styles} view={view} />
                    )) ||
                    (status === "paused" && <PausedMessage l10n={l10n} styles={styles} view={view} isPage={isPage} />) || (
                        <ClosedMessage l10n={l10n} styles={styles} view={view} isPage={isPage} />
                    )}
            </AutoscrollRoot>
        </StrictMode>
    );
};
