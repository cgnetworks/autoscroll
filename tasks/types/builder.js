const fs = require("fs");
const banner = require("../banner/banner.js");

fs.mkdirSync("./builder/types/", { recursive: true });
fs.writeFileSync(
    "./builder/types/index.d.ts",
    `/*! ${banner} */\n\ndeclare module "tripetto-runner-autoscroll/builder" {\nexport {};\n}\n`,
    "utf8"
);
