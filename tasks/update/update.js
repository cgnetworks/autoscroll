const fs = require("fs");
const pkg = require("../../package.json");
const prettier = require("prettier");

pkg.peerDependencies["react"] = ">= 16.14.x";
pkg.peerDependencies["react-dom"] = ">= 16.x";
pkg.peerDependencies["tripetto-runner-foundation"] = pkg.devDependencies["tripetto-runner-foundation"];

fs.writeFileSync("./package.json", prettier.format(JSON.stringify(pkg), { parser: "json-stringify" }), "utf8");
